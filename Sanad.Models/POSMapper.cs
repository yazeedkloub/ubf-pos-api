﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class POSMapper
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int EstablishmentId { get; set; }
        [Required(ErrorMessage = "Please Provide a POS Name")]
        [StringLength(250)]
        public string Name { get; set; }

        public string IPadUDID { get; set; }

        public string PrinterId { get; set; }

        public PrinterType? PrinterType { get; set; }

        public string PrinterTypeStr  => PrinterType?.ToString();



        public UserMapper User { get; set; }

        public EstablishmentMapper  Establishment { get; set; }


    }
}
