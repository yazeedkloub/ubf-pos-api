﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class QuotationItemMapper
    {

        public int Id { get; set; }
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public float UnitPrice { get; set; }
        public float CostPrice { get; set; }
        public float Discount { get; set; }
        public float Tax { get; set; }

    }
}
