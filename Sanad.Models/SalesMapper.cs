﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanad.Entities;

namespace Sanad.Models
{
    public class SalesMapper
    {
        public int Id { get; set; }
        public int POSId { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public float DiscountAmount { get; set; }
        public float DiscountPercentage { get; set; }
        public string DiscountReason { get; set; }
        public float TotalQuantity { get; set; }
        public float TotalItems { get; set; }
        public List<CartMapper> CartItems { get; set; }
        public List<PaymentMapper> Payments { get; set; }
        public string CustomerBarcode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTRN { get; set; }
        public string TransactionId { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float TotalAmount { get; set; }
        public Status Status { get; set; }

        public int? QuotationNumber { get; set; }


        public string StatusStr  => Status.ToString();


        public DateTime UpdatedAt { get; set; }


    }
}
