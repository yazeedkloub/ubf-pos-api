﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class UserTypeCreateMapper
    {
        [Required(ErrorMessage = "Please Provide a User Type Arabic Name")]
        [StringLength(250)]
        public string ArabicName { get; set; }
        [Required(ErrorMessage = "Please Provide a User Type English Name")]
        [StringLength(250)]
        public string EnglishName { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}
