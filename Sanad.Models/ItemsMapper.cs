﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class ItemsMapper
    {
        public int Id { get; set; }
        public int? EstablishmentId { get; set; }
        [Required(ErrorMessage = "Please Provide a Item Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public int? CategoryId { get; set; }

        public CategoryMapper Category { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public string BarcodeName { get; set; }
        public float Price { get; set; }
        public float Discount { get; set; }
        public string ItemId { get; set; }

        public float Quantity { get; set; }

    }
}
