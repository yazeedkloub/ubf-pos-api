﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sanad.Models
{
    public class UserCreateMapper
    {


        [Required]
        [EmailAddress]
        [StringLength(250)]
        public string Username { get; set; }
        [Required]
        [StringLength(250)]
        public string FirstName { get; set; }
        [StringLength(250)]
        public string LastName { get; set; }

        [Required]
        [StringLength(250)]
        public string Permissions { get; set; }

        public string Companies { get; set; }



    }
}
