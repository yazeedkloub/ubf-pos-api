﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class UserSystemConfigurationMapper
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public int? ParentId { get; set; }

        public List<UserSystemConfigurationMapper> Children { get; set; }
    }
}
