﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanad.Entities;

namespace Sanad.Models
{
    public class SalesCreateMapper
    {
        public int POSId { get; set; }
        public string Comment { get; set; }
        public float DiscountAmount { get; set; }
        public float DiscountPercentage { get; set; }
        public string DiscountReason { get; set; }
        public float TotalQuantity { get; set; }
        public float TotalItems { get; set; }
        public List<CartCreateMapper> CartItems { get; set; }
        public List<PaymentCreateMapper> Payments { get; set; }
        public string CustomerBarcode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTRN { get; set; }
        public string TransactionId { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float TotalAmount { get; set; }
        public Status Status { get; set; }
        public int? QuotationNumber { get; set; }


        public DateTime CreatedAt { get; set; } = DateTime.Now;

    }

    public class ListSalesCreateMapper
    {
        public List<SalesCreateMapper> BulkItems { get; set; }
    }
}
