﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class CompanyContactCreateMapper
    {
        [Required(ErrorMessage = "Please Provide a Contact Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Designation { get; set; }
   
    }
}
