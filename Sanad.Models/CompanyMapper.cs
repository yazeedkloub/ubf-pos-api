﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class CompanyMapper
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Provide a Comapny Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string TRN { get; set; }

        public List<CompanyContactMapper> CompanyContacts { get; set; }



    }
}
