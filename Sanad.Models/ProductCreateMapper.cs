﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class ProductCreateMapper
    {
        public int EstablishmentId { get; set; }
        [Required(ErrorMessage = "Please Provide a Product Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string BarcodeName { get; set; }
        //[ForeignKey("FK_Product_ID_Category")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CategoryId { get; set; }
        public float FixedCost { get; set; }
        public float Price { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}
