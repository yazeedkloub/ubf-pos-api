﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class PaymentMapper
    {
        public int Id { get; set; }
        public string PaymentType { get; set; }
        public string PaymentAmount { get; set; }
    }
}
