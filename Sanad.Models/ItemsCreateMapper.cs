﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class ItemsCreateMapper
    {

        public int? EstablishmentId { get; set; }

        [Required(ErrorMessage = "Please Provide a Item Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public int? CategoryId { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public string BarcodeName { get; set; }
        public float Price { get; set; }
        public float Discount { get; set; }

        [Required(ErrorMessage = "Please Provide a Item Id")]
        public string ItemId { get; set; }

        //public bool Yeild { get; set; }
        //public bool NotTracked { get; set; }
        //public float YeildPercentage { get; set; }
        //public float Quantity { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;





    }

    public class ListItemCreateMapper
    {
        public List<ItemsCreateMapper> BulkItems { get; set; }

    }


}
