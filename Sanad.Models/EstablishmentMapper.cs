﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class EstablishmentMapper
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        [Required(ErrorMessage = "Please Provide a Establishment Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Address { get; set; } //{object}
        public string City { get; set; }
        public string State { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }


        public bool Taxable { get; set; }

        public TaxType? TaxType { get; set; }

        public string TaxTypeStr  => TaxType?.ToString();


        public string Currency { get; set; }

        public string Logo { get; set; }

        public bool HasVariableItem { get; set; }

        public string ColorCode { get; set; }



        public CompanyMapper Company { get; set; }


        public List<EstablishmentContactMapper> EstablishmentContacts { get; set; }
    }
}
