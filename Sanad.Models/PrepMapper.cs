﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class PrepMapper
    {
        public int Id { get; set; }
        public int EstablishmentId { get; set; }
        [Required(ErrorMessage = "Please Provide a Prep Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string BarcodeName { get; set; }
        public int CategoryId { get; set; }
        public float FixedCost { get; set; }
        public float DynamicCost { get; set; }
        public float Quinatity { get; set; }
        public int QuantityUnitId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
