﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class CategoryMapper
    {

        public int Id { get; set; }
     //   public int EstablishmentId { get; set; }
        [Required(ErrorMessage = "Please Provide a Cateory Arabic Name")]
        [StringLength(250)]
        public string ArabicName { get; set; }
        [Required(ErrorMessage = "Please Provide a Cateory English Name")]
        [StringLength(250)]
        public string EnglishName { get; set; }

        //public Category Parent { get; set; }
        //public int? parent_id { get; set; }
        //public string color { get; set; }
        //public string category_info_popup { get; set; }
        //public string image_url { get; set; }
       // public bool IsDeleted { get; set; } = false;
    

    }     
}
