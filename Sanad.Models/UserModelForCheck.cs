﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class UserModelForCheck
    {
        //[Required]
        //[StringLength(250)]
        //public string Username { get; set; }
        [Required]
        [StringLength(250)]
        public string Password { get; set; }
        [Required]
        public int RequestId { get; set; }
    }
}
