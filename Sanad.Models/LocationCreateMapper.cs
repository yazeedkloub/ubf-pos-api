﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class LocationCreateMapper
    {
        public int POSId { get; set; }
        [Required(ErrorMessage = "Please Provide a Location Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Address { get; set; }
        public string Company { get; set; }
        public string taxId { get; set; }
        //public string phone { get; set; }
        ////////////////////////////////
        public bool IsDeleted { get; set; } = false;
        //public DateTime CreatedAt { get; set; } = DateTime.Now;
        //public DateTime UpdatedAt { get; set; } = DateTime.Now;

    }
}
