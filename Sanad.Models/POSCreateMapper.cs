﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class POSCreateMapper
    {

        public int UserId { get; set; }

        [Required(ErrorMessage = "Please Provide a POS Name")]
        [StringLength(250)]
        public string Name { get; set; }


        public string IPadUDID { get; set; }

        public string PrinterId { get; set; }

        public PrinterType? PrinterType { get; set; }

        public string PrinterTypeStr => PrinterType?.ToString();



    }
}
