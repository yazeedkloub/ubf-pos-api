﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class ProductIngredientsMapper
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int PrepId { get; set; }
        public float PrepQuinatityRequired { get; set; }
        public Item Item { get; set; }
        public float ItemQuinatityRequired { get; set; }
        //public bool YeildItemRequired { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
