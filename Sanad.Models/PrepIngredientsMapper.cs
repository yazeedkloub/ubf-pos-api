﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class PrepIngredientsMapper
    {
        public int Id { get; set; }
        public int PrepId { get; set; }
        //[ForeignKey("FK_PrepIngredients_ID_Item")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Item Item { get; set; }
        public float QuinatityRequired { get; set; }
        public bool YeildItemRequired { get; set; }
        //[ForeignKey("FK_PrepIngredients_ID_QuinatityUnit")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
