﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class QuotationCreateMapper
    {
        public int Id { get; set; }
        public int POSId { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public float DiscountAmount { get; set; }
        public float DiscountPercentage { get; set; }
        public string DiscountReason { get; set; }
        public float TotalQuantity { get; set; }
        public float TotalItems { get; set; }
        public List<QuotationItemCreateMapper> CartItems { get; set; }
        public string CustomerBarcode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTRN { get; set; }
        public string TransactionId { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float TotalAmount { get; set; }
     


        public DateTime CreatedAt { get; set; }
    }
}
