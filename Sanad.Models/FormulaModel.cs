﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class FormulaModel
    {

        public List<Condition> Conditions { get; set; }

        public DiscountType DiscountType { get; set; }

        public float Discount { get; set; }



    }

    public class Condition
    {
        public ConditionType ConditionType { get; set; }
        public float Value { get; set; }
    }


    public enum DiscountType
    {
        Fixed = 1,
        Percentage
    }

    public enum ConditionType
    {
        [EnumMember(Value = "Equals")]
        Equals = 1,
        [EnumMember(Value = "GreaterThan")]
        GreaterThan,
        [EnumMember(Value = "GreaterEquals")]
        GreaterEquals,
        [EnumMember(Value = "LessThan")]
        LessThan,
        [EnumMember(Value = "LessEquals")]
        LessEquals
    }
}
