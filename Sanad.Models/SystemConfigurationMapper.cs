﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class SystemConfigurationMapper
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string NameEn { get; set; }

        public string NameAr { get; set; }


        public int? ParentId { get; set; }

        public string Code { get; set; }



        public List<SystemConfigurationMapper> Children { get; set; }
    }
}
