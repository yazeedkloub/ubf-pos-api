﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class CreateEditSystemConfigurationMapper
    {

        [StringLength(250)]
        public string NameEn { get; set; }

        [StringLength(250)]
        public string NameAr { get; set; }
        public int? ParentId { get; set; } = null;

        public string Code { get; set; }

    }
}
