﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanad.Entities;

namespace Sanad.Models
{
    public class InventoryMapper
    {
        public List<int> categoryId { get; set; }
        public int POSId { get; set; }
        public List<string> categoryName { get; set; }
        public List<int> SaleCount { get; set; }
    }
}
