﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class FormulaResultModelMapper
    {
        public int Count { get; set; }

        public List<ItemsCreateMapper> Result { get; set; }
    }
}
