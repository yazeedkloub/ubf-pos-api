﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Models
{
    public class ProductIngredientsCreateMapper
    {
        public int ProductId { get; set; }
        //[ForeignKey("FK_ProductIngredients_ID_Prep")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PrepId { get; set; }
        //[ForeignKey("FK_ProductIngredients_ID_Item")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public float PrepQuinatityRequired { get; set; }
        public Item Item { get; set; }
        public float ItemQuinatityRequired { get; set; }
        //public bool YeildItemRequired { get; set; }
        //[ForeignKey("FK_PrepIngredients_ID_QuinatityUnit")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public bool IsDeleted { get; set; } = false;
    }
}
