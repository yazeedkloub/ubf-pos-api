﻿using System.Threading.Tasks;

namespace Sanad.Api.ApiResult
{
    public class Result : IResult
    {
        public Result()
        {
        }

        public string Messages { get; set; }

        public bool Succeeded { get; set; }

        public int Code { get; set; }


        public static IResult Fail()
        {
            return new Result { Succeeded = false };
        }

      

        public static IResult Fail(string messages)
        {
            return new Result { Succeeded = false, Messages = messages };
        }

        public static  async Task<IResult> FailAsync()
        {
            return await Task.FromResult(Fail());
        }

        public static IResult Fail(string messages,int code)
        {
            return new Result { Succeeded = false, Messages = messages,Code = code };
        }

        public static Task<IResult> FailAsync(string messages)
        {
            return Task.FromResult(Fail(messages));
        }
        public static Task<IResult> FailAsync(string messages, int code)
        {
            return Task.FromResult(Fail(messages,code));
        }
        public static IResult Success()
        {
            return new Result { Succeeded = true };
        }

        public static IResult Success(string message)
        {
            return new Result { Succeeded = true, Messages =message  };
        }

        public static Task<IResult> SuccessAsync()
        {
            return Task.FromResult(Success());
        }

        public static Task<IResult> SuccessAsync(string message)
        {
            return Task.FromResult(Success(message));
        }
    }

    public class Result<T> : Result, IResult<T>
    {
        public Result()
        {
        }

        public T Data { get; set; }

        public new static Result<T> Fail()
        {
            return new Result<T> { Succeeded = false };
        }

    

        public new static Result<T> Fail(string messages,int code =0)
        {
            return new Result<T> { Succeeded = false, Messages = messages,Code = code };
        }

        public new static Task<Result<T>> FailAsync()
        {
            return Task.FromResult(Fail());
        }

    

        public new static Task<Result<T>> FailAsync(string messages)
        {
            return Task.FromResult(Fail(messages));
        }

        public new static Result<T> Success()
        {
            return new Result<T> { Succeeded = true };
        }

        public static Result<T> Success(string message,int code = 0)
        {
            return new Result<T> { Succeeded = true, Messages =  message,Code = code  };
        }

        public static Result<T> Success(T data,int code = 0)
        {
            return new Result<T> { Succeeded = true, Data = data,Code = code };
        }

      

        public static Result<T> Success(T data, string messages,int code = 0)
        {
            return new Result<T> { Succeeded = true, Data = data, Messages = messages,Code = code };
        }

        public  static Task<Result<T>> SuccessAsync(string message,int code = 0)
        {
            return Task.FromResult(Success(message,code));
        }

        public static Task<Result<T>> SuccessAsync(T data)
        {
            return Task.FromResult(Success(data));
        }

        public static Task<Result<T>> SuccessAsync(T data, string message,int code = 0)
        {
            return Task.FromResult(Success(data, message,code));
        }
    }

}
