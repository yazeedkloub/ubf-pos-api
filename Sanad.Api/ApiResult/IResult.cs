﻿namespace Sanad.Api.ApiResult
{
    public interface IResult
    {
        string Messages { get; set; }

        bool Succeeded { get; set; }

        public int Code { get; set; }
    }

    public interface IResult<out T> : IResult
    {
        T Data { get; }
    }
}
