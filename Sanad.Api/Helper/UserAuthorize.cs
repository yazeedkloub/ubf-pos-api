﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Sanad.Services;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Helpers;

namespace Sanad.Api.Helper
{
    public class UserAuthorize : TypeFilterAttribute
    {
        public UserAuthorize(string permission = "") : base(typeof(UserAuthorizeFilter))
        {
            Arguments = new string[] { permission };
        }

        public class UserAuthorizeFilter : IAsyncActionFilter
        {
            readonly string _permission;
            public UserAuthorizeFilter(string permission = "")
            {
                _permission = permission;
            }

          

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next )
            {
                try
                {

                   var userService =  context.HttpContext.RequestServices.GetService(typeof(IUserRepository)) as IUserRepository;
                   var appSettings = context.HttpContext.RequestServices.GetService(typeof(IOptions<AppSettings>)) as IOptions<AppSettings>;


                    var token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                    if (!string.IsNullOrEmpty(token) && context?.HttpContext?.User?.Identity.IsAuthenticated is true)
                    {
                        
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
                    tokenHandler.ValidateToken(token, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                        ClockSkew = TimeSpan.Zero
                    }, out SecurityToken validatedToken);

                    var jwtToken = (JwtSecurityToken)validatedToken;
                        if (jwtToken?.Claims?.Any(x => x != null && x.Type == "id") is true && int.TryParse(jwtToken?.Claims?.First(x => x != null && x.Type == "id")?.Value, out int userId))
                        {
                            var user = await userService.First(u => u.Id == userId);

                            if (user == null && userId == 0)
                            {
                                user = await userService.First(u => u.IsDeleted == false);

                            }
                            if (user == null)
                            {
                                context.HttpContext.Response.StatusCode = 401;
                                return;
                            }

                            if (string.IsNullOrEmpty(user.Permissions) || !user.Permissions.Split(',').Any(p => p == _permission || p == "1"))
                            {
                                context.HttpContext.Response.StatusCode = 403;
                                return;
                            }

                        }
                          
                    }
                 
                    await next();



                }
                catch (Exception e)
                {
                    context.HttpContext.Response.StatusCode = 507;

                    var result = Encoding.UTF8.GetBytes(e.Message);

                    context.HttpContext.Response.Body.Write(result, 0, result.Length);
                    return;
                }

            }

        }
    }
}
