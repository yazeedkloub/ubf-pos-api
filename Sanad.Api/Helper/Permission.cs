﻿namespace Sanad.Api.Helper
{
    public class Permission
    {
        public const string Admin ="1";
        public const string User = "2";
        public const string Category = "3";
        public const string Company = "4";
        public const string Establishment = "5";
        public const string POS = "6";
        public const string Item = "7";
        public const string Sale = "8";

    }
}
