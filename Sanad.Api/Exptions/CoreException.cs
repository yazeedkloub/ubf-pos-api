﻿using System;
using System.Net;

namespace Sanad.Api.Exptions
{
    public class CoreException : Exception
    {

        public HttpStatusCode Code { get; set; }

        public CoreException() : base()
        {

        }

        public CoreException(string message) : base(message)
        {

        }

        public CoreException(HttpStatusCode code) : base()
        {
            this.Code = code;
        }

        public CoreException(HttpStatusCode code, string message) : base(message)
        {
            this.Code = code;
        }
    }
}
