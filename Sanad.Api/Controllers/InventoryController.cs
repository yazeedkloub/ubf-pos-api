﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Sanad.Api.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class InventoryController : Controller
    {

        private readonly IInventoryRepository _inventory;
        private readonly IMapper _mapper;

        public InventoryController(IInventoryRepository inventory, IMapper mapper)
        {
            _inventory = inventory;
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult<InventoryMapper[]>> GetItemsCategory()
        {
            try
            {
                var result = await _inventory.GetItemsCategory();
                //return Ok(_mapper.Map<SalesMapper[]>(result));
                return Ok(result);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        //[HttpGet("{id}")]
        //public async Task<ActionResult<SalesMapper[]>> GetById(int SaleId)
        //{
        //    try
        //    {
        //        var result = await _sales.GetSalesByIdAsync(SaleId);
        //        //return Ok(_mapper.Map<SalesMapper[]>(result));
        //        return Ok(result);
        //    }
        //    catch (Exception)
        //    {
        //        return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
        //    }
        //}

        //[HttpPost]
        //public async Task<IActionResult> Post(SalesCreateMapper salesCreateMapper)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var newSales = _mapper.Map<Sales>(salesCreateMapper);
        //            _sales.AddSales(newSales);
        //            if (await _sales.SaveChangesAsync())
        //            {
        //                var addedSale = await _sales.GetSaleLastId();
        //                return Ok(new { addedSale });
        //            }
        //            return Ok(new { status = "error" });
        //        }
        //        else
        //        {
        //            return BadRequest(ModelState);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
        //    }
        //}
        //[HttpPut("{id:int}")]
        //public async Task<ActionResult<SalesMapper>> Put(int id, SalesCreateMapper salesCreateMapper)
        //{
        //    try
        //    {
        //        var oldSale = await _sales.GetSalesByIdAsync(id);
        //        if (oldSale == null) return NotFound("Sales Not Found");
        //        _mapper.Map(salesCreateMapper, oldSale);
        //        if (await _sales.SaveChangesAsync())
        //        {
        //            return Ok(_mapper.Map<SalesMapper>(oldSale));
        //        }
        //        return BadRequest();
        //    }
        //    catch (Exception)
        //    {
        //        return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
        //    }
        //}
        //[HttpDelete("{id:int}")]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    try
        //    {
        //        var sales = await _sales.GetSalesByIdAsync(id);
        //        if (sales == null) return NotFound("Sales Not Found");
        //        sales.IsDeleted = true;
        //        if (await _sales.SaveChangesAsync())
        //        {
        //            return Ok();
        //        }
        //        return BadRequest();
        //    }
        //    catch (Exception)
        //    {
        //        return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
        //    }
        //}

    }
}
