﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Services.Filters;
using Sanad.Api.Helper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace Sanad.Api.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Item)]

    public class ItemsController : BaseController
    {

        private readonly IItemRepository _items;
        private readonly IMapper _mapper;
        private readonly IEstablishmentRepository  _establishmentRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;



        public ItemsController(IItemRepository items, IMapper mapper, IEstablishmentRepository establishmentRepository, ICategoryRepository categoryRepository, IHttpContextAccessor httpContextAccessor):base(httpContextAccessor)
        {
            _items = items;
            _mapper = mapper;
            _establishmentRepository = establishmentRepository;
            this.categoryRepository = categoryRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<ItemsMapper[]>> Get([FromQuery]ItemFilter filter)
        {
            try
            {
                
                    filter.CompanyIds = CompanyIds;

                var result = await _items.Filter(filter);//FilterProjection(filter).ProjectTo<ItemsMapper>(_mapper.ConfigurationProvider).ToListAsync();
                return await Task.FromResult(Ok(_mapper.Map< ItemsMapper[]>(result.Result)));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ItemsMapper>> Get( int id)
        {
            try
            {
                var result = await _items.GetItemsByIdAsync(id);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<ItemsMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

        [HttpGet("GetDiscount")]
        public async Task<ActionResult<ItemsMapper>> GetDiscount()
        {
           
                var result = await _items.GetItemByNameAsync(null,"Discount");
                if (result == null) return NotFound();
                return Ok(_mapper.Map<ItemsMapper>(result));
          
        }

        [HttpPost("UpdateDiscount")]
        public async Task<ActionResult> UpdateDiscount(List<FormulaModel> listFormula)
        {


            if (listFormula?.Any() is false)
                return BadRequest("Invalid formula list");

            //Check if there is no condition
            if(listFormula.Any(f => f.Conditions == null || !f.Conditions.Any()) )
                return BadRequest("Invalid Conditions");

            //Check if there us discount more than price
            if( listFormula.Any(f => f.DiscountType == DiscountType.Fixed &&  f.Conditions.Any(c => c.Value < f.Discount)))
            {
             
                    return BadRequest("The discount value must be less than or equal to the condition value");

            }
            var result = new List<FormulaResultModelMapper>();
           foreach(var formula in listFormula)
            {
                result.Add(_mapper.Map<FormulaResultModelMapper>(await _items.UpdateDiscount(formula)));
            }

            return Ok(result);

        }
        [HttpPost("bulk")]
        public async Task<ActionResult<List<ItemsMapper>>> PostBulk(ListItemCreateMapper listItemsCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var itemsCreateMapperList = listItemsCreateMapper?.BulkItems;


                    if (itemsCreateMapperList == null)
                        return BadRequest("Invalid request");

                    var itemList = new List<Item>();
                   foreach(var itemsCreateMapper in  itemsCreateMapperList)
                    {
                        if (itemsCreateMapper.EstablishmentId.HasValue)
                        {
                            var establishment = await _establishmentRepository.GetEstablishmentByIdAsync(itemsCreateMapper.EstablishmentId.Value);

                            if (establishment == null)
                                return BadRequest("This Establishment not exist");


                            if (await _items.Any(i => i.ItemId == itemsCreateMapper.ItemId && i.EstablishmentId == itemsCreateMapper.EstablishmentId))
                                return BadRequest("Item Id " + itemsCreateMapper.ItemId + " already exist");
                        }

                        if (itemsCreateMapper.CategoryId.HasValue)
                        {
                            var category = await categoryRepository.GetCategoryByIdAsync(itemsCreateMapper.CategoryId.Value);

                            if (category == null)
                                return BadRequest("This Category not exist");
                        }

                        var existing = await _items.GetItemByNameAsync(itemsCreateMapper.EstablishmentId, itemsCreateMapper.Name);
                        if (existing != null) return BadRequest("Item "+itemsCreateMapper.Name+" already exist");

                        var item = _mapper.Map<Item>(itemsCreateMapper);
                        item.EstablishmentId = itemsCreateMapper.EstablishmentId;
                        item.CreatedBy = CurrentUserId;
                        _items.AddItems(item);
                        itemList.Add(item);
                    }
                   
                    if (await _items.SaveChangesAsync())
                    {
                        var listResult = await _items.Filter(new ItemFilter()
                        {
                            Ids = itemList.Select(i => i.Id).ToList(),
                            GetAll = true
                        });
                        return Ok(_mapper.Map<List<ItemsMapper>>(listResult.Result));
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<ActionResult<ItemsMapper>> Post( ItemsCreateMapper itemsCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    if(itemsCreateMapper.EstablishmentId.HasValue)
                    {
                        var establishment = await _establishmentRepository.GetEstablishmentByIdAsync(itemsCreateMapper.EstablishmentId.Value);

                        if (establishment == null)
                            return BadRequest("This Establishment not exist");

                        if(await _items.Any(i => i.ItemId == itemsCreateMapper.ItemId && i.EstablishmentId == itemsCreateMapper.EstablishmentId))
                            return BadRequest("Item Id "+itemsCreateMapper.ItemId+" already exist");
                    }

                    if(itemsCreateMapper.CategoryId.HasValue)
                    {
                        var category = await categoryRepository.GetCategoryByIdAsync(itemsCreateMapper.CategoryId.Value);

                        if (category == null)
                            return BadRequest("This Category not exist");
                    }

                    var existing = await _items.GetItemByNameAsync(itemsCreateMapper.EstablishmentId, itemsCreateMapper.Name);
                    if (existing != null) return BadRequest("This Item already exist");
                    var item = _mapper.Map<Item>(itemsCreateMapper);
                    item.EstablishmentId = itemsCreateMapper.EstablishmentId;
                    item.CreatedBy = CurrentUserId;
                    _items.AddItems(item);
                    if (await _items.SaveChangesAsync())
                    {
                        var addedItem = await _items.GetItemByNameAsync(itemsCreateMapper.EstablishmentId, itemsCreateMapper.Name);
                        return Ok( _mapper.Map<ItemsMapper>(addedItem) );
                    }
                    return Ok(new { status = "error" });
                }
                else {
                    return BadRequest(ModelState);
                }
               
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

        [HttpPost("AddTest")]
        public async Task<ActionResult> AddTest(int count)
        {
            try
            {

                var items = await _items.Filter(new ItemFilter()
                {
                    GetAll = true
                });
             
                for(int i= 0;i<count;i++)
                {
                    foreach (var item in items.Result)
                    {
                        var newItem = _mapper.Map<Item>(item);
                        newItem.Id = 0;
                        newItem.Name = item.Name + item.Id.ToString();
                        _items.AddItems(newItem);

                    }
                }

                await _items.SaveChangesAsync();

                return Ok();




            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
            return await Task.FromResult(Ok());



        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<ItemsMapper>> Put(int id, ItemsCreateMapper itemMapper)
        {
            try
            {
                var oldItem = await _items.GetItemsByIdAsync(id);
                if (oldItem == null) return NotFound("Item not found");
                
                if(itemMapper.EstablishmentId.HasValue)
                {
                    var establishment = await _establishmentRepository.GetEstablishmentByIdAsync(itemMapper.EstablishmentId.Value);

                    if (establishment == null)
                        return BadRequest("This Establishment not exist");

                    if (await _items.Any(i => i.Id != id && i.ItemId == itemMapper.ItemId && i.EstablishmentId == itemMapper.EstablishmentId))
                        return BadRequest("Item Id " + itemMapper.ItemId + " already exist");
                }

             
                if(itemMapper.CategoryId.HasValue)
                {
                    var category = await categoryRepository.GetCategoryByIdAsync(itemMapper.CategoryId.Value);

                    if (category == null)
                        return BadRequest("This Category not exist");
                }
                _mapper.Map(itemMapper, oldItem);
                oldItem.UpdatedAt = DateTime.Now;
                oldItem.UpdatedBy = CurrentUserId;
                if (await _items.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<ItemsMapper>(oldItem));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete( int id)
        {
            try
            {
                var item = await _items.GetItemsByIdAsync(id);
                if (item  == null) return NotFound("Item not found");
                

               
                item.IsDeleted = true;
                item.UpdatedAt = DateTime.Now;
                item.DeletedBy = CurrentUserId;

                if (await _items.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
