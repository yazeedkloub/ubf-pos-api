﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Services.Filters;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Sale)]

    public class SalesController : BaseController
    {

        private readonly ISalesRepository _sales;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IItemRepository _itemRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPOSRepository _posRepository;

        public SalesController(ISalesRepository sales, IMapper mapper, IHttpContextAccessor contextAccessor, IItemRepository itemRepository, IUserRepository userRepository, IPOSRepository posRepository) : base(contextAccessor)
        {
            _sales = sales;
            _mapper = mapper;
            _contextAccessor = contextAccessor;
            _itemRepository = itemRepository;
            _userRepository = userRepository;
            _posRepository = posRepository;
        }

        [HttpGet]
        public async Task<ActionResult<SalesMapper[]>> Get([FromQuery] SaleFilter filter)
        {

            var result = await _sales.Filter(filter);
            return Ok(_mapper.Map<SalesMapper[]>(result));

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SalesMapper>> GetById(int id)
        {

            var result = await _sales.GetSalesByIdAsync(id);
            return Ok(_mapper.Map<SalesMapper>(result));

        }
        [HttpPost("bulk")]
        public async Task<IActionResult> Post(ListSalesCreateMapper listSalesCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var salesCreateMapperList = listSalesCreateMapper?.BulkItems;

                    if (salesCreateMapperList == null)
                        return BadRequest();


                    var listresult = new List<Sales>();
                    foreach (SalesCreateMapper salesCreateMapper in salesCreateMapperList)
                    //Check pos
                    {
                        var pos = await _posRepository.First(pos => pos.Id == salesCreateMapper.POSId);
                        if (pos == null)
                            return BadRequest("POS not exist");

                        if (salesCreateMapper.CartItems == null || !salesCreateMapper.CartItems.Any())

                            return BadRequest("Cart items is empty");

                        salesCreateMapper.CartItems.ForEach( cartItem =>
                        {
                            if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                                cartItem.ItemId = ( _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Result?.Id;
                        });
                        //Check if item list is valid
                        var itemIds = salesCreateMapper.CartItems.Select(c => c.ItemId).Distinct().ToList();
                        var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                        if (items.Count() != itemIds.Count)
                        {
                            var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).ToList().Contains(i.Value));

                            return BadRequest("Item " + notValidId.ToString() + " not exist");
                        }

                        var newSales = _mapper.Map<Sales>(salesCreateMapper);
                        newSales.CreatedBy = CurrentUserId;
                        _sales.AddSales(newSales);
                        listresult.Add(newSales);
                    }

                    if (await _sales.SaveChangesAsync())
                    {
                        var addedSaleList = await _sales.Filter(new SaleFilter()
                        {
                            Ids = listresult.Select(s => s.Id).ToList(),
                            GetAll = true
                        });
                        return Ok(_mapper.Map<List<SalesMapper>>(addedSaleList));
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(SalesCreateMapper salesCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Check pos
                    var pos = await _posRepository.First(pos => pos.Id == salesCreateMapper.POSId);
                    if (pos == null)
                        return BadRequest("POS not exist");

                    if (salesCreateMapper.CartItems == null || !salesCreateMapper.CartItems.Any())

                        return BadRequest("Cart items is empty");

                    salesCreateMapper.CartItems.ForEach( cartItem =>
                    {
                        if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                            cartItem.ItemId = ( _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Result?.Id;
                    });
                    //Check if item list is valid
                    var itemIds = salesCreateMapper.CartItems.Select(c => c.ItemId).Distinct().ToList();
                    var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                    if (items.Count() != itemIds.Count)
                    {
                        var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).ToList().Contains(i.Value));

                        return BadRequest("Item " + notValidId.ToString() + " not exist");
                    }

                    var newSales = _mapper.Map<Sales>(salesCreateMapper);
                    newSales.CreatedBy = CurrentUserId;
                    _sales.AddSales(newSales);
                    if (await _sales.SaveChangesAsync())
                    {
                        var addedSale = await _sales.GetSaleLastId();
                        return Ok(_mapper.Map<SalesMapper>(addedSale));
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<SalesMapper>> Put(int id, SalesCreateMapper salesCreateMapper)
        {
            try
            {
                var oldSale = await _sales.GetSalesByIdAsync(id);
                if (oldSale == null) return NotFound("Sales Not Found");

                var pos = await _posRepository.First(pos => pos.Id == salesCreateMapper.POSId);
                if (pos == null)
                    return BadRequest("POS not exist");

                if (salesCreateMapper.CartItems == null || !salesCreateMapper.CartItems.Any())

                    return BadRequest("Cart items is empty");
                salesCreateMapper.CartItems.ForEach(async cartItem =>
                {
                    if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                        cartItem.ItemId = (await _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Id;
                });


                var itemIds = salesCreateMapper.CartItems.Select(c => c.ItemId).ToList();
                var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                if (items.Count() != itemIds.Count)
                {
                    var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).Contains(i.Value));

                    return BadRequest("Item " + notValidId.ToString() + " not exist");
                }
                _mapper.Map(salesCreateMapper, oldSale);
                _sales.Update(oldSale);

                oldSale.UpdatedBy = CurrentUserId;
                if (await _sales.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<SalesMapper>(oldSale));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

        [HttpPut("ChangeStatus/{id:int}")]
        public async Task<ActionResult<SalesMapper>> ChangeStatus(int id, Status status)
        {
            try
            {
                var oldSale = await _sales.GetSalesByIdAsync(id);
                if (oldSale == null) return NotFound("Sales Not Found");

                oldSale.Status = status;
                oldSale.UpdatedAt = DateTime.Now;
                oldSale.UpdatedBy = CurrentUserId;
                if (await _sales.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<SalesMapper>(oldSale));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var sales = await _sales.GetSalesByIdAsync(id);
                if (sales == null) return BadRequest("Sales Not Found");
                //sales.IsDeleted = true;
                sales.Status = Status.Voided;
                sales.UpdatedAt = DateTime.Now;
                sales.DeletedBy = CurrentUserId;
                if (await _sales.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }


    }
}
