﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Sanad.Api.Controllers
{
    [Route("api/{EstablishmentId:int}/[controller]")]
    [ApiController]
    [Authorize]
    public class EstablishmentContactsController : BaseController
    {

        private readonly IEstablishmentContactRepository _establishmentContact;
        private readonly IMapper _mapper;
        private readonly IEstablishmentRepository _establishment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EstablishmentContactsController(IEstablishmentContactRepository establishmentContact, IMapper mapper, IEstablishmentRepository establishment, IHttpContextAccessor httpContextAccessor):base(httpContextAccessor)
        {
            _establishmentContact = establishmentContact;
            _mapper = mapper;
            _establishment = establishment;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<EstablishmentContactMapper[]>> Get(int EstablishmentId)
        {
            try
            {
                var result = await _establishmentContact.GetAllEstablishmentContactAsync(EstablishmentId);
                return Ok(_mapper.Map<EstablishmentContactMapper[]>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<EstablishmentContactMapper>> Get(int id, int EstablishmentId)
        {
            try
            {
                var result = await _establishmentContact.GetEstablishmentContactByIdAsync(id, EstablishmentId);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<EstablishmentContactMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(int EstablishmentId, EstablishmentContactCreateMapper establishmentContactMaper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var establishment = await _establishment.GetEstablishmentByIdAsync(EstablishmentId);

                    if (establishment == null)
                        return BadRequest("This Establishment not exist");

                    var existing = await _establishmentContact.GetEstablishmentContactByNameAsync(EstablishmentId, establishmentContactMaper.Name);
                    if (existing != null) return BadRequest("This Establishment Contact already exist");
                    var establishmentContact = _mapper.Map<EstablishmentContact>(establishmentContactMaper);
                    establishmentContact.EstablishmentId = EstablishmentId;
                    _establishmentContact.AddEstablishmentContact(establishmentContact);
                    if (await _establishmentContact.SaveChangesAsync())
                    {
                        return Ok(new { status = "Successfully saved" });
                    }
                    return Ok(new { status = "error" });
                }
                else {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<EstablishmentContactMapper>> Put(int EstablishmentId, int id, EstablishmentContactCreateMapper establishmentContactMapper)
        {
            try
            {
                var establishment = await _establishment.GetEstablishmentByIdAsync(EstablishmentId);

                if (establishment == null)
                    return BadRequest("This Establishment not exist");
                var oldEstablishmentContact = await _establishmentContact.GetEstablishmentContactByIdAsync(EstablishmentId, id);
                if (oldEstablishmentContact == null) return NotFound("This Establishment Contact not exist");
                _mapper.Map(establishmentContactMapper, oldEstablishmentContact);
                oldEstablishmentContact.UpdatedAt = DateTime.Now;

                if (await _establishmentContact.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<EstablishmentContactMapper>(oldEstablishmentContact));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int EstablishmentId, int id)
        {
            try
            {
                var establishment = await _establishment.GetEstablishmentByIdAsync(EstablishmentId);

                if (establishment == null)
                    return BadRequest("This Establishment not exist");

                var establishmentContact = await _establishmentContact.GetEstablishmentContactByIdAsync(EstablishmentId, id);
                if (establishmentContact == null) return NotFound("This Establishment Contact not exist");
                establishmentContact.IsDeleted = true;
                establishmentContact.UpdatedAt = DateTime.Now;

                if (await _establishmentContact.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
