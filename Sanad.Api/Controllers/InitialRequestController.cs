﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Sanad.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class InitialRequestController : BaseController
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InitialRequestController(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]

        public async Task<ActionResult> Get()
        {
            return await Task.FromResult(Ok(new { IsOk = true,DateTime = DateTime.Now}));
        }
    }
}
