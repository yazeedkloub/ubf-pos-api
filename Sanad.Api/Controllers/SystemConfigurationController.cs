﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Api.Helper;
using Sanad.Entities;
using Sanad.Models;
using Sanad.Services;
using Sanad.Services.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sanad.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Admin)]

    public class SystemConfigurationController : BaseController
    {
        private readonly ISystemConfigurationRepository _systemConfiguration;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SystemConfigurationController(ISystemConfigurationRepository systemConfiguration, IMapper mapper, IHttpContextAccessor httpContextAccessor):base(httpContextAccessor)
        {
            _systemConfiguration = systemConfiguration;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<ActionResult<List<SystemConfigurationMapper>>> Get()
        {
           
                var result = await _systemConfiguration.GetAllAsync();
                return Ok(_mapper.Map<List<SystemConfigurationMapper>>(result));
            
          
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<SystemConfigurationMapper>> Get(int id)
        {
            
                var result = await _systemConfiguration.GetByIdAsync(id);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<SystemConfigurationMapper>(result));
          
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateEditSystemConfigurationMapper createModel)
        {
           
               
                if (createModel.ParentId is > 0  && !(await _systemConfiguration.Any(s => s.Id == createModel.ParentId)))
                {
                    return BadRequest("Parent not exists");
                }

            if ((await _systemConfiguration.Any(s => s.Code == createModel.Code)))
            {
                return BadRequest("Code already exists");
            }
            var configuration = _mapper.Map<SystemConfiguration>(createModel);
                _systemConfiguration.Add(configuration);
            configuration.CreatedAt = DateTime.Now;
            configuration.CreatedBy = CurrentUserId;
                if (await _systemConfiguration.SaveAsync())
                {
                    return Ok(new { status = "successfully saved" });
                }
                return Ok(new { status = "error" });
            
           
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<SystemConfigurationMapper>> Put(int id, CreateEditSystemConfigurationMapper editModel)
        {
            
                var oldSystemConfiguration = await _systemConfiguration.GetByIdAsync(id);
                if (oldSystemConfiguration == null)
                {
                    return NotFound("SystemConfiguration not Found");
                }
                if (editModel.ParentId is > 0 && !(await _systemConfiguration.Any(s => s.Id == editModel.ParentId)))
                    return BadRequest("Parent not exists");
            if ((await _systemConfiguration.Any(s => s.Id != id && s.Code == editModel.Code)))
            {
                return BadRequest("Code already exists");
            }

            _mapper.Map(editModel, oldSystemConfiguration);
            oldSystemConfiguration.UpdatedAt = DateTime.Now;

            oldSystemConfiguration.UpdatedBy = CurrentUserId;
            if (await _systemConfiguration.SaveAsync())
                {
                    return Ok(_mapper.Map<SystemConfigurationMapper>(oldSystemConfiguration));
                }
                return BadRequest();
            
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            
                var configuration = await _systemConfiguration.GetByIdAsync(id);
                if (configuration == null) return NotFound("System configuration Not Found");
            //_user.RemoveUser(user);
            _systemConfiguration.Remove(configuration);
                if (await _systemConfiguration.SaveAsync())
                {
                    return Ok();
                }
                return BadRequest();
            
           
        }

        [HttpGet("Filter")]
        public async Task<ActionResult<SystemConfigurationMapper[]>> Filter([FromQuery] SystemConfigurationFilter filter)
        {

            var result = await _systemConfiguration.Filter(filter);
            return Ok(_mapper.Map<SystemConfigurationMapper[]>(result.Result));


        }
    }
}
