﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Services.Filters;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Sale)]

    public class QuotationController : BaseController
    {

       // private readonly ISalesRepository _sales;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IItemRepository _itemRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPOSRepository _posRepository;
        private readonly IQuotationRepository _quotation;


        public QuotationController(ISalesRepository sales, IMapper mapper, IHttpContextAccessor contextAccessor, IItemRepository itemRepository, IUserRepository userRepository, IPOSRepository posRepository, IQuotationRepository quotation) : base(contextAccessor)
        {
          //  _sales = sales;
            _mapper = mapper;
            _contextAccessor = contextAccessor;
            _itemRepository = itemRepository;
            _userRepository = userRepository;
            _posRepository = posRepository;
            _quotation = quotation;
        }

        [HttpGet]
        public async Task<ActionResult<QuotationMapper[]>> Get([FromQuery] QuotationFilter filter)
        {
           
                var result = await _quotation.Filter(filter);
                return Ok(_mapper.Map<QuotationMapper[]>(result));
         
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<QuotationMapper>> GetById(int id)
        {
           
                var result = await _quotation.GetQuotationByIdAsync(id);
                return Ok(_mapper.Map<QuotationMapper>(result));
            
        }
        [HttpPost("bulk")]
        public async Task<IActionResult> Post(List<QuotationCreateMapper> quotationCreateMapperList)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Check pos
                
                    if(quotationCreateMapperList == null)
                        return BadRequest();


                    var listresult = new List<Quotation>();
                    foreach(QuotationCreateMapper quotationCreateMapper in quotationCreateMapperList)
                    {
                        var pos = await _posRepository.First(pos => pos.Id == quotationCreateMapper.POSId);
                        if (pos == null)
                            return BadRequest("POS not exist");

                        if (quotationCreateMapper.CartItems == null || !quotationCreateMapper.CartItems.Any())

                            return BadRequest("Cart items is empty");

                        quotationCreateMapper.CartItems.ForEach(async cartItem =>
                        {
                            if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                                cartItem.ItemId = (await _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Id;
                        });
                        //Check if item list is valid
                        var itemIds = quotationCreateMapper.CartItems.Select(c => c.ItemId).ToList();
                        var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                        if (items.Count() != itemIds.Count)
                        {
                            var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).ToList().Contains(i.Value));

                            return BadRequest("Item " + notValidId.ToString() + " not exist");
                        }

                        var newQuotation = _mapper.Map<Quotation>(quotationCreateMapper);
                        newQuotation.CreatedBy = CurrentUserId;
                        _quotation.AddQuotation(newQuotation);
                        listresult.Add(newQuotation);
                    }

                    if (await _quotation.SaveChangesAsync())
                    {
                        var addedQuotationList = await _quotation.Filter(new QuotationFilter()
                        {
                            Ids = listresult.Select(s => s.Id).ToList(),
                            GetAll = true
                        });
                        return Ok(_mapper.Map<List<QuotationMapper>>(addedQuotationList )) ;
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(QuotationCreateMapper quotationCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Check pos
                    var pos = await _posRepository.First(pos => pos.Id == quotationCreateMapper.POSId);
                    if(pos == null)
                        return BadRequest("POS not exist");

                    if(quotationCreateMapper.CartItems == null || !quotationCreateMapper.CartItems.Any())
                    
                        return BadRequest("Cart items is empty");

                    quotationCreateMapper.CartItems.ForEach(async cartItem =>
                    {
                        if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                            cartItem.ItemId = (await _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Id;
                    });
                    //Check if item list is valid
                    var itemIds = quotationCreateMapper.CartItems.Select(c => c.ItemId).ToList();
                    var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                    if(items.Count() != itemIds.Count)
                    {
                        var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).ToList().Contains(i.Value));

                        return BadRequest("Item " + notValidId.ToString() + " not exist");
                    }

                    var newQuotation = _mapper.Map<Quotation>(quotationCreateMapper);
                    newQuotation.CreatedBy = CurrentUserId;
                    _quotation.AddQuotation(newQuotation);
                    if (await _quotation.SaveChangesAsync())
                    {
                        var addedQuotation = await _quotation.GetQuotationLastId();
                        return Ok( _mapper.Map<QuotationMapper>(addedQuotation) );
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<QuotationMapper>> Put(int id, QuotationCreateMapper quotationCreateMapper)
        {
            try
            {
                var oldQuotation = await _quotation.GetQuotationByIdAsync(id);
                if (oldQuotation == null) return NotFound("Quotation Not Found");

                var pos = await _posRepository.First(pos => pos.Id == quotationCreateMapper.POSId);
                if (pos == null)
                    return BadRequest("POS not exist");

                if (quotationCreateMapper.CartItems == null || !quotationCreateMapper.CartItems.Any())

                    return BadRequest("Cart items is empty");
                quotationCreateMapper.CartItems.ForEach(async cartItem =>
                {
                    if (!cartItem.ItemId.HasValue || cartItem.ItemId.Value == 0)
                        cartItem.ItemId = (await _itemRepository.First(i => i.ItemId == cartItem.ItemIdStr))?.Id;
                });


                var itemIds = quotationCreateMapper.CartItems.Select(c => c.ItemId).ToList();
                var items = await _itemRepository.GetWhere(i => itemIds.Contains(i.Id));
                if (items.Count() != itemIds.Count)
                {
                    var notValidId = itemIds.FirstOrDefault(i => !items.Select(existItem => existItem.Id).Contains(i.Value));

                    return BadRequest("Item " + notValidId.ToString() + " not exist");
                }
                _mapper.Map(quotationCreateMapper, oldQuotation);
                _quotation.Update(oldQuotation);

                oldQuotation.UpdatedBy = CurrentUserId;
                if (await _quotation.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<QuotationMapper>(oldQuotation));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

   
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var quotation = await _quotation.GetQuotationByIdAsync(id);
                if (quotation == null) return BadRequest("Quotation Not Found");
                //sales.IsDeleted = true;
            
                quotation.UpdatedAt = DateTime.Now;
                quotation.DeletedBy = CurrentUserId;
                if (await _quotation.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

    }
}
