﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Entities;
using Sanad.Services;
using System.Collections.Generic;
using System.Linq;

namespace Sanad.Api.Controllers
{

    public class BaseController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BaseController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            if ( _httpContextAccessor?.HttpContext?.Items["User"] is User user)
            {
                CurrentUser = user;
                if (user != null)
                    CurrentUserId = user.Id;
            }else if(_httpContextAccessor?.HttpContext?.User?.Identity.IsAuthenticated is true)
            {
                int userId = 0;
                if(_httpContextAccessor.HttpContext.User.FindFirst(c => c.Type == "id")?.Value is string id && int.TryParse(id,out userId) )
                {
                    CurrentUserId = userId;
                   if( _httpContextAccessor.HttpContext.RequestServices.GetService(typeof(IUserRepository)) is IUserRepository userRepository )
                    {
                        if (userRepository != null)
                            CurrentUser = userRepository.GetUserByIdAsync(userId).Result;

                        if(!string.IsNullOrEmpty(CurrentUser.Companies))
                        {
                            var companysList = CurrentUser.Companies.Split(",").Select(c => int.TryParse(c, out int id) ? id : 0).Where(c => c > 0).ToList();
                            CompanyIds = new List<int>();
                            CompanyIds.AddRange(companysList);
                        }

                    }
                }
            }
        }

        protected User CurrentUser { get; private set; }

        protected int? CurrentUserId { get; private set; }

        protected List<int> CompanyIds { get; private set; }


        [HttpOptions]
        public ActionResult<string> Option()
        {
            string username = HttpContext.User.Identity.Name;
            return username;
        }



    }
}
