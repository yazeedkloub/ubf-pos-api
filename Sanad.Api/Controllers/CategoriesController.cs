﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Category)]
    public class CategoriesController : BaseController
    {

        private readonly ICategoryRepository _category;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public CategoriesController(ICategoryRepository category, IMapper mapper, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _category = category;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<CategoryMapper[]>> Get()
        {

            var result = await _category.GetAllCategoryAsync();
            return Ok(_mapper.Map<CategoryMapper[]>(result));


        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CategoryMapper>> Get(int id)
        {

            var result = await _category.GetCategoryByIdAsync(id);
            if (result == null) return NotFound();
            return Ok(_mapper.Map<CategoryMapper>(result));


        }
        [HttpPost]
        public async Task<IActionResult> Post(CategoryCreateMapper categoryMaper)
        {

            if (ModelState.IsValid)
            {
                var existing = await _category.GetCategoryByNameAsync(categoryMaper.ArabicName, categoryMaper.EnglishName);
                if (existing != null) return BadRequest("This Category already exist");
                var category = _mapper.Map<Category>(categoryMaper);
                category.CreatedBy = CurrentUserId;
                _category.AddCategory(category);
                if (await _category.SaveChangesAsync())
                {
                    return Ok(new { status = "Successfully saved" });
                }
                return Ok(new { status = "error" });
            }
            else
            {
                return BadRequest(ModelState);
            }

        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<CategoryMapper>> Put(int id, CategoryCreateMapper categoryMapper)
        {

            var oldCategory = await _category.GetCategoryByIdAsync(id);
            if (oldCategory == null) return NotFound("This Category not exist");
            _mapper.Map(categoryMapper, oldCategory);
            oldCategory.UpdatedBy = CurrentUserId;
            if (await _category.SaveChangesAsync())
            {
                return Ok(_mapper.Map<CategoryMapper>(oldCategory));
            }
            return BadRequest();

        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {

            var category = await _category.GetCategoryByIdAsync(id);
            if (category == null) return NotFound("This category not exist");
            category.IsDeleted = true;
            category.DeletedBy = CurrentUserId;
            if (await _category.SaveChangesAsync())
            {
                return Ok();
            }
            return BadRequest();

        }
    }
}
