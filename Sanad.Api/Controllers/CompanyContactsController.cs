﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Sanad.Api.Controllers
{
    [Route("api/{CompanyId:int}/[controller]")]
    [ApiController]
    [Authorize]
    public class CompanyContactsController : BaseController
    {

        private readonly ICompanyContactRepository _companyContact;
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public CompanyContactsController(ICompanyContactRepository companyContact, IMapper mapper, ICompanyRepository companyRepository, IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
            _companyContact = companyContact;
            _mapper = mapper;
            _companyRepository = companyRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<CompanyContactMapper[]>> Get(int CompanyId)
        {
            try
            {
              
                var result = await _companyContact.GetAllCompanyContactAsync(CompanyId);
                return Ok(_mapper.Map<CompanyContactMapper[]>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CompanyContactMapper>> Get(int id, int CompanyId)
        {
            try
            {
                var result = await _companyContact.GetCompanyContactByIdAsync(id, CompanyId);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<CompanyContactMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(int CompanyId, CompanyContactCreateMapper companyContactMaper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                    if (company == null)
                        return BadRequest("This Company not exist");
                    var existing = await _companyContact.GetCompanyContactByNameAsync(CompanyId, companyContactMaper.Name);
                    if (existing != null) return BadRequest("This Company Contact already exist");
                    var companyContact = _mapper.Map<CompanyContact>(companyContactMaper);
                    companyContact.CompanyId= CompanyId;
                    _companyContact.AddCompanyContact(companyContact);
                    if (await _companyContact.SaveChangesAsync())
                    {
                        return Ok(new { status = "Successfully saved" });
                    }
                    return Ok(new { status = "error" });
                }
                else {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<CompanyContactMapper>> Put(int CompanyId, int id, CompanyContactCreateMapper companyContactMapper)
        {
            try
            {
                var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                if (company == null)
                    return BadRequest("This Company not exist");
                var oldCompanyContact = await _companyContact.GetCompanyContactByIdAsync(CompanyId, id);
                if (oldCompanyContact == null) return NotFound("This Company Contact not exist");
                _mapper.Map(companyContactMapper, oldCompanyContact);
                oldCompanyContact.UpdatedAt = DateTime.Now;

                if (await _companyContact.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<CompanyContactMapper>(oldCompanyContact));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int CompanyId, int id)
        {
            try
            {
                var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                if (company == null)
                    return BadRequest("This Company not exist");
                var companyContact = await _companyContact.GetCompanyContactByIdAsync(CompanyId, id);
                if (companyContact == null) return NotFound("This Company Contact not exist");
                companyContact.IsDeleted = true;
                companyContact.UpdatedAt = DateTime.Now;

                if (await _companyContact.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
