﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Company)]

    public class CompaniesController : BaseController
    {

        private readonly ICompanyRepository _company;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CompaniesController(ICompanyRepository company, IMapper mapper, IHttpContextAccessor httpContextAccessor) :base(httpContextAccessor)
        {
            _company = company;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<CompanyMapper[]>> Get()
        {
            try
            {
                
                var result = await _company.GetAllCompanyAsync(CurrentUser?.Companies?.Split(",").ToList());
                return Ok(_mapper.Map<CompanyMapper[]>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CompanyMapper>> Get(int id)
        {
            try
            {
                var result = await _company.GetCompanyByIdAsync(id);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<CompanyMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(CompanyCreateMapper companyMaper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existing = await _company.GetCompanyByNameAsync(companyMaper.Name);
                    if (existing != null) return BadRequest("This Company already exist");
                    var company = _mapper.Map<Company>(companyMaper);
                    company.CreatedBy = CurrentUserId;
                    _company.AddCompany(company);
                    if (await _company.SaveChangesAsync())
                    {
                        return Ok(new { status = "Successfully saved" });
                    }
                    return Ok(new { status = "error" });
                }
                else {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<CompanyMapper>> Put(int id, CompanyCreateMapper companyMapper)
        {
            try
            {
                var oldCompany = await _company.GetCompanyByIdAsync(id);
                if (oldCompany == null) return NotFound("This Company not exist");
                _mapper.Map(companyMapper, oldCompany);
                oldCompany.UpdatedAt = DateTime.Now;
                oldCompany.UpdatedBy = CurrentUserId;
                if (await _company.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<CompanyMapper>(oldCompany));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var company = await _company.GetCompanyByIdAsync(id);
                if (company == null) return NotFound("This company not exist");
                company.IsDeleted = true;
                company.UpdatedAt = DateTime.Now;
                company.DeletedBy = CurrentUserId;

                if (await _company.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
