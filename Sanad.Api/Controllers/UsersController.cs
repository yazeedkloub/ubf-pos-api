﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Threading.Tasks;
using Sanad.Services.Filters;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;
using Sanad.Api.Helper;
using System.Linq;
using System.Collections.Generic;

namespace Sanad.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.User)]

    public class UsersController : BaseController
    {
        private readonly IUserRepository _user;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserKeyRepository _userKeyRepository;
        private readonly ILoginLogRepository _loginLogRepository;
        private readonly IPOSRepository _posRepository;
        private readonly ISystemConfigurationRepository _systemConfiguration;






        public IConfiguration Configuration { get; }
        public UsersController(IUserRepository user, IMapper mapper, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IUserKeyRepository userKeyRepository, ILoginLogRepository loginLogRepository, IPOSRepository posRepository, ISystemConfigurationRepository systemConfiguration) : base(httpContextAccessor)
        {
            _user = user;
            _mapper = mapper;
            Configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _userKeyRepository = userKeyRepository;
            _loginLogRepository = loginLogRepository;
            _posRepository = posRepository;
            _systemConfiguration = systemConfiguration;
        }



        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserMapper>> Get(int id)
        {
            try
            {

                var user = CurrentUser;
                var result = await _user.GetUserByIdAsync(id);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<UserMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }


        [HttpGet("AuthKey")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> AuthKey(string userName)
        {

            var result = await _userKeyRepository.Generate(userName);
            if (result == null) return NotFound();
            return Ok(new { Key = result.Key, RequestId = result.Id });

        }
        [HttpPost]
        public async Task<IActionResult> Post(UserCreateMapper createModel)
        {

            var existing = await _user.GetUserByUsernameAsync(createModel.Username);
            if (existing != null)
            {
                return BadRequest("This email already exist");
            }



            var user = _mapper.Map<User>(createModel);
            user.Password = BCrypt.Net.BCrypt.HashPassword("12345678");

            _user.AddUser(user);
            user.CreatedBy = CurrentUserId;
            if (await _user.SaveAsync())
            {
                return Ok(new { status = "successfully saved" });
            }
            return Ok(new { status = "error" });

        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserMapper>> Put(int id, UserCreateMapper user)
        {

            var oldUser = await _user.GetUserByIdAsync(id);
            if (oldUser == null)
            {
                return NotFound("User not Found");
            }
            if (oldUser.Username != user.Username && await _user.Any(u => u.Username == user.Username))
                return BadRequest("This email already exist");



            _mapper.Map(user, oldUser);
            oldUser.UpdatedBy = CurrentUserId;

            if (await _user.SaveAsync())
            {
                return Ok(_mapper.Map<UserMapper>(oldUser));
            }
            return BadRequest();

        }

        [HttpPut("ResetPassword")]
        [AllowAnonymous]
        public async Task<ActionResult<UserMapper>> ResetPassword([EmailAddress] string email, string newPassword)
        {

            var oldUser = await _user.GetUserByUsernameAsync(email);
            if (oldUser == null)
            {
                return NotFound("User not Found");
            }

            oldUser.Password = newPassword;
            if (await _user.SaveAsync())
            {
                return Ok(_mapper.Map<UserMapper>(oldUser));
            }
            return BadRequest();

        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {

            var user = await _user.GetUserByIdAsync(id);
            if (user == null) return NotFound("User Not Found");
            //_user.RemoveUser(user);
            user.IsDeleted = true;
            user.UpdatedAt = DateTime.Now;
            user.UpdatedBy = CurrentUserId;

            if (await _user.SaveAsync())
            {
                return Ok();
            }
            return BadRequest();

        }
        [HttpPut("Active/{id:int}")]
        public async Task<IActionResult> Active(int id)
        {

            var user = await _user.GetUserByIdAsync(id);
            if (user == null) return NotFound("User Not Found");
            //_user.RemoveUser(user);
            user.IsActive = !user.IsActive;
            user.UpdatedAt = DateTime.Now;
            user.UpdatedBy = CurrentUserId;



            if (await _user.SaveAsync())
            {
                return Ok();
            }
            return BadRequest();

        }
        [HttpGet]
        public async Task<ActionResult<UserMapper[]>> Filter([FromQuery] UserFilter filter)
        {

            var result = await _user.Filter(filter);
            return Ok(_mapper.Map<UserMapper[]>(result.Result));


        }

        [HttpPost("SetOnline")]
        public async Task<ActionResult<string>> SetOnline(string ipadUDID)
        {
            if (ipadUDID == null || !(await _posRepository.Any(p => p.IPadUDID.ToLower() == ipadUDID.ToLower())))
                return BadRequest("Invalid IPdad UDID");

            var loginLog = await _loginLogRepository.First(l => l.IPadUDID.ToLower() == ipadUDID.ToLower());

            if (loginLog == null)
            {
                _loginLogRepository.Add(new LoginLog()
                {
                    UserId = CurrentUserId,
                    IPadUDID = ipadUDID
                });
            }
            else
            {
                loginLog.UserId = CurrentUserId;
                loginLog.UpdatedAt = DateTime.Now;
            }
            await _loginLogRepository.SaveChanges();

            return await Task.FromResult(Ok());

        }

        [HttpGet("RefreshToken")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> RefreshToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:key"])),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero,
                    ValidateLifetime = false
                }, out SecurityToken validatedToken);
                var expireValue = int.TryParse(Configuration["Jwt:ExpirationHours"] ?? "24", out int value) ? value : 0;

                var expireDate = expireValue > 0 ? DateTime.Now.AddHours(expireValue) : DateTime.Now.AddMinutes(30);


                if (validatedToken == null && validatedToken.ValidTo.AddHours(expireValue).AddDays(1) >= DateTime.Now)
                    return BadRequest("Invalid token");


                var jwtToken = (JwtSecurityToken)validatedToken;


                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(jwtToken.Claims),
                    Issuer = Configuration["JWT:Issuer"],
                    Audience = Configuration["JWT:Audience"],
                    Expires = expireDate,
                    SigningCredentials = new SigningCredentials(
                     new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:key"])),
                     "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                     "http://www.w3.org/2001/04/xmlenc#sha256")




                };
                var newToken = tokenHandler.CreateToken(tokenDescriptor);

                var tokenString = tokenHandler.WriteToken(newToken);

                return await Task.FromResult(Ok(new { Token = tokenString, TokenExpirationDate = newToken.ValidTo, }));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(BadRequest("Invalid token"));
            }




        }


        [HttpPost("Profile")]
        public async Task<IActionResult> Profile()
        {
            User userInfo = await _user.GetUserByIdAsync(CurrentUserId??0);
            if (userInfo == null || userInfo.IsDeleted) return NotFound();
            if (!userInfo.IsActive)
                return Forbid();

            if (!string.IsNullOrEmpty(userInfo.Permissions))
            {

                var permissionList = userInfo.Permissions.Split(',').Select(c => int.TryParse(c, out int id) ? id : 0).Where(c => c > 0).ToList();
                var systemConfigList = await _systemConfiguration.GetAllAsync(permissionList);
                return Ok(new { Profile = _mapper.Map<UserMapper>(userInfo), UserPermissions = _mapper.Map<List<UserSystemConfigurationMapper>>(systemConfigList) });
            }
            return Ok(new {  Profile = _mapper.Map<UserMapper>(userInfo) });
        }
            [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserModelForCheck user)
        {
            User userInfo = await _user.GetUserByUserNameAndPassowrdAsync(user.Password, user.RequestId);
            if (userInfo == null || userInfo.IsDeleted) return NotFound();
            if (!userInfo.IsActive)
                return Forbid();

            var expireDate = int.TryParse(Configuration["Jwt:ExpirationHours"] ?? "24", out int value) && value > 0 ? DateTime.Now.AddHours(value) : DateTime.Now.AddMinutes(30);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                       {
                            new Claim("id", userInfo.Id.ToString()),
                       }),
                Issuer = Configuration["JWT:Issuer"],
                Audience = Configuration["JWT:Audience"],
                Expires = expireDate,
                SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:key"])),
                        "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                        "http://www.w3.org/2001/04/xmlenc#sha256")




            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var tokenString = tokenHandler.WriteToken(token);
            _userKeyRepository.Delete(user.RequestId);
            await _userKeyRepository.SaveChanges();
            if (!string.IsNullOrEmpty(userInfo.Permissions))
            {

                var permissionList = userInfo.Permissions.Split(',').Select(c => int.TryParse(c, out int id) ? id : 0).Where(c => c > 0).ToList();
                var systemConfigList = await _systemConfiguration.GetAllAsync(permissionList);
                return Ok(new { Token = tokenString, TokenExpirationDate = token.ValidTo, Profile = _mapper.Map<UserMapper>(userInfo), UserPermissions = _mapper.Map<List<UserSystemConfigurationMapper>>(systemConfigList) });
            }
            return Ok(new { Token = tokenString, TokenExpirationDate = token.ValidTo, Profile = _mapper.Map<UserMapper>(userInfo) });
        }
    }
}
