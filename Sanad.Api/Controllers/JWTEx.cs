﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymphonyGroup.API.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private IConfiguration _config;

        public TokenController(IConfiguration config)
        {
            _config = config;
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody] LoginModel login)
        {
            IActionResult response = Unauthorized();
            var user = Authenticate(login);

            if (user != null)
            {
                var token = BuildToken(user);
                response = Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token), TokenExpirationDate = token.ValidTo });
            }

            return response;
        }
        private JwtSecurityToken BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: int.TryParse(_config["Jwt:ExpirationHours"]??"24" , out int value) && value > 0 ? DateTime.Now.AddHours(value)  : DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return token;
        }
        private UserModel Authenticate(LoginModel login)
        {
            UserModel user = null;

            if (login.Username == _config["Jwt:Username"]  && login.Password == _config["Jwt:Password"])
            {
                user = new UserModel { Name = "API Admin", Email = "ykloub@dctabudhabi.ae" };
            }
            return user;
        }
        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        private class UserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime Birthdate { get; set; }
        }
    }
}
