﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.POS)]

    public class POSController : BaseController
    {

        private readonly IPOSRepository _pos;
        private readonly IMapper _mapper;
        private readonly IEstablishmentRepository _establishment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IUserRepository _userRepository;


        public POSController(IPOSRepository pos, IMapper mapper, IEstablishmentRepository establishment, IHttpContextAccessor httpContextAccessor, IUserRepository userRepository) : base(httpContextAccessor)
        {
            _pos = pos;
            _mapper = mapper;
            _establishment = establishment;
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
        }
        [HttpGet]
        public async Task<ActionResult<POSMapper[]>> Get(int EstablishmentId)
        {
            try
            {
                var result = await _pos.GetAllPOSAsync(EstablishmentId);
                return Ok(_mapper.Map<POSMapper[]>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<POSMapper>> Get(int EstablishmentId, int id)
        {
            try
            {
                var result = await _pos.GetPOSByIdAsync(EstablishmentId, id);
                if (result == null) return NotFound();

                return Ok(_mapper.Map<POSMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }

        [HttpGet("GetByUDID")]

        public async Task<ActionResult<POSMapper>> GetByUDID(string udid)
        {
            try
            {
                var result = await _pos.GetPOSByUDIDAsync(udid);
                if (result == null) return NotFound();

                return Ok(_mapper.Map<POSMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(int EstablishmentId, POSCreateMapper POSCreateMapper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existing = await _pos.GetPOSByNameAsync(EstablishmentId, POSCreateMapper.Name);
                    if (existing != null) return BadRequest("This POS already exist");
                    var establishment = await _establishment.GetEstablishmentByIdAsync(EstablishmentId);

                    if (establishment == null)
                        return BadRequest("This Establishment not exist");


                    if (!(await _userRepository.Any(u => u.Id == POSCreateMapper.UserId)))
                        return BadRequest("This User not exist");

                    if (await _pos.Any(p => p.UserId == POSCreateMapper.UserId && p.IsDeleted == false))
                        return BadRequest("This User already assign");
         


                    if (!string.IsNullOrEmpty(POSCreateMapper.IPadUDID) && await _pos.Any(p => p.IPadUDID == POSCreateMapper.IPadUDID))
                        return BadRequest("This Mac Adress already exist");

                    var pos = _mapper.Map<POS>(POSCreateMapper);
                    pos.EstablishmentId = EstablishmentId;
                    pos.CreatedBy = CurrentUserId;
                    _pos.AddPOS(pos);

                    if (await _pos.SaveChangesAsync())
                    {
                        var addedPOS = await _pos.GetPOSByNameAsync(EstablishmentId, POSCreateMapper.Name);
                        return Ok(_mapper.Map<POSMapper>(addedPOS));
                    }
                    return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [Route("bulk")]
        [HttpPost]
        public async Task<IActionResult> PostBulk(int EstablishmentId, List<POSCreateMapper> POSCreateMapperList)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int count = 0;
                    int exist = 0;
                    string existName = "";
                    var establishment = await _establishment.GetEstablishmentByIdAsync(EstablishmentId);

                    if (establishment == null)
                        return BadRequest("This Establishment not exist");
                    foreach (POSCreateMapper pOSCreateMapper in POSCreateMapperList)
                    {
                        var existing = await _pos.GetPOSByNameAsync(EstablishmentId, pOSCreateMapper.Name);
                        if (existing != null)
                        {
                            existName += pOSCreateMapper.Name + " | "; exist += 1;
                        }
                        if (await _pos.Any(p => p.UserId == pOSCreateMapper.UserId && p.IsDeleted == false))
                            return BadRequest("UserId:"+pOSCreateMapper.UserId+" already assign");



                        if (!string.IsNullOrEmpty(pOSCreateMapper.IPadUDID) && await _pos.Any(p => p.IPadUDID == pOSCreateMapper.IPadUDID))
                            return BadRequest("This IPadUDID already exist");
                        else
                        {
                            count += 1;
                            var pos = _mapper.Map<POS>(pOSCreateMapper);

                            _pos.AddPOS(pos);

                            pos.EstablishmentId = EstablishmentId;
                            pos.CreatedBy = CurrentUserId;
                            if (await _pos.SaveChangesAsync())
                            {
                                var addedPOS = await _pos.GetPOSByNameAsync(EstablishmentId, pOSCreateMapper.Name);
                            }
                        }
                    }
                    return Ok(new { AddedCount = count, AlreadyExistCount = exist, AlreadyExistName = existName });
                    //return Ok(new { status = "error" });
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<POSMapper>> Put(int EstablishmentId, int id, POSCreateMapper itemMapper)
        {
            try
            {
                var oldItem = await _pos.GetPOSByIdAsync(EstablishmentId, id);
                if (oldItem == null) return NotFound("POS not found");

                if (!string.IsNullOrEmpty(itemMapper.IPadUDID) && await _pos.Any(p => p.Id != id && p.IPadUDID == itemMapper.IPadUDID))
                    return BadRequest("This Mac Adress already exist");


                if (!(await _userRepository.Any(u => u.Id == itemMapper.UserId)))
                    return BadRequest("This User not exist");

                if (await _pos.Any(p => p.Id != id && p.UserId == itemMapper.UserId && p.IsDeleted == false))
                    return BadRequest("This User already assign");
                _mapper.Map(itemMapper, oldItem);
                oldItem.UpdatedAt = DateTime.Now;
                oldItem.UpdatedBy = CurrentUserId;
                if (await _pos.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<POSMapper>(oldItem));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int EstablishmentId, int id)
        {
            try
            {
                var item = await _pos.GetPOSByIdAsync(EstablishmentId, id);
                if (item == null) return NotFound("POS not found");
                item.IsDeleted = true;
                item.UpdatedAt = DateTime.Now;
                item.DeletedBy = CurrentUserId;
                item.UserId = null;
                if (await _pos.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
