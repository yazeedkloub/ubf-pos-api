﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sanad.Services;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Sanad.Api.Helper;

namespace Sanad.Api.Controllers
{
    [Route("api/{CompanyId:int}/[controller]")]
    [ApiController]
    [Authorize]
    [UserAuthorize(Permission.Establishment)]

    public class EstablishmentsController : BaseController
    {

        private readonly IEstablishmentRepository _establishment;
        private readonly ICompanyRepository _companyRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;


        private readonly IMapper _mapper;

        public EstablishmentsController(IEstablishmentRepository establishment, IMapper mapper, ICompanyRepository companyRepository, IHttpContextAccessor httpContextAccessor):base(httpContextAccessor)
        {
            _establishment = establishment;
            _mapper = mapper;
            _companyRepository = companyRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        public async Task<ActionResult<EstablishmentMapper[]>> Get(int CompanyId)
        {
            try
            {
                var result = await _establishment.GetAllEstablishmentAsync(CompanyId);
                return Ok(_mapper.Map<EstablishmentMapper[]>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<EstablishmentMapper>> Get(int id, int CompanyId)
        {
            try
            {
                var result = await _establishment.GetEstablishmentByIdAsync(id, CompanyId);
                if (result == null) return NotFound();
                return Ok(_mapper.Map<EstablishmentMapper>(result));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post(int CompanyId, EstablishmentCreateMapper establishmentMaper)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                    if (company == null)
                        return BadRequest("This Company not exist");
                    var existing = await _establishment.GetEstablishmentByNameAsync(CompanyId, establishmentMaper.Name);
                    if (existing != null) return BadRequest("This Establishment already exist");
                    var establishment = _mapper.Map<Establishment>(establishmentMaper);
                    establishment.CompanyId = CompanyId;
                    establishment.CreatedBy = CurrentUserId;
                    _establishment.AddEstablishment(establishment);
                    if (await _establishment.SaveChangesAsync())
                    {
                        return Ok(new { status = "Successfully saved" });
                    }
                    return Ok(new { status = "error" });
                }
                else {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<EstablishmentMapper>> Put(int CompanyId, int id, EstablishmentCreateMapper establishmentMapper)
        {
            try
            {
                var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                if (company == null)
                    return BadRequest("This Company not exist");
                var oldEstablishment = await _establishment.GetEstablishmentByIdAsync(CompanyId, id);
                if (oldEstablishment == null) return NotFound("This Establishment not exist");
                _mapper.Map(establishmentMapper, oldEstablishment);
                oldEstablishment.UpdatedAt = DateTime.Now;
                oldEstablishment.UpdatedBy = CurrentUserId;

                if (await _establishment.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<EstablishmentMapper>(oldEstablishment));
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int CompanyId, int id)
        {
            try
            {
                var company = await _companyRepository.GetCompanyByIdAsync(CompanyId);

                if (company == null)
                    return BadRequest("This Company not exist");
                var establishment = await _establishment.GetEstablishmentByIdAsync(CompanyId, id);
                if (establishment == null) return NotFound("This establishment not exist");
                establishment.IsDeleted = true;
                establishment.UpdatedAt = DateTime.Now;
                establishment.DeletedBy = CurrentUserId;

                if (await _establishment.SaveChangesAsync())
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Faild");
            }
        }
    }
}
