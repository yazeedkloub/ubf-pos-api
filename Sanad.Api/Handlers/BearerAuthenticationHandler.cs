﻿using Microsoft.AspNetCore.Cors;
using Microsoft.IdentityModel.Tokens;
using Sanad.Entities;
using Sanad.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Api.Handlers
{
    [EnableCors("MyPolicy")]
    public class BearerAuthenticationHandler : IBearerAuthenticationHandler
    {
        private readonly string _tokenKey;
        private readonly IUserRepository _user;

        public BearerAuthenticationHandler( string tokenKey, IUserRepository user)
        {
            _tokenKey = tokenKey;
            _user = user;
        }

        public async Task<string> Authenticate(string username, string password)
        {
            User user = await _user.GetUserByUsernameAsync(username);
            if (user != null)
            {
                bool verified = BCrypt.Net.BCrypt.Verify(password, user.Password);
                if (verified)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_tokenKey);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[] {
                            new Claim(ClaimTypes.Name, username)
                        }),
                        Expires = DateTime.UtcNow.AddHours(1),
                        SigningCredentials = new SigningCredentials(
                            new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    return tokenHandler.WriteToken(token);
                }
                return null;
            }
            return null;
        }
    }
}
