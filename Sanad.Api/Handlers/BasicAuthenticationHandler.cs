﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sanad.Services;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;

namespace Sanad.Api.Handlers
{

    [EnableCors("MyPolicy")]

    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IUserRepository _user;

        public BasicAuthenticationHandler(
            IUserRepository user,
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            _user = user;
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                if (!Request.Headers.ContainsKey("Authorization"))
                    return AuthenticateResult.Fail("Authorization header was not found");
                var authenticationHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var bytes = Convert.FromBase64String(authenticationHeaderValue.Parameter);
                string[] credential = Encoding.UTF8.GetString(bytes).Split(":");
                string username = credential[0];
                string password = credential[1];
                //User user = await _user.CheckUser(username, password);
                User user = await _user.GetUserByUsernameAsync(username);
                if (user != null)
                {
                    bool verified = BCrypt.Net.BCrypt.Verify(password, user.Password);
                    if (verified)
                    {
                        var claims = new[] { new Claim(ClaimTypes.Name, user.Username) };
                        var identity = new ClaimsIdentity(claims, Scheme.Name);
                        var principal = new ClaimsPrincipal(identity);
                        var ticket = new AuthenticationTicket(principal, Scheme.Name);
                        return AuthenticateResult.Success(ticket);
                        //return AuthenticateResult.Success("kk");
                    }
                    return AuthenticateResult.Fail("Invalid username or password");
                }
                return AuthenticateResult.Fail("Invalid username or password");
            }
            catch (Exception)
            {
                return AuthenticateResult.Fail("Error");
            }
        }
    }
}
