﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Handlers
{
    public interface IBearerAuthenticationHandler
    {
        Task<string> Authenticate(string username, string password);
    }
}

