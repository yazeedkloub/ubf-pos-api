﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class CompanyContactProfile : Profile
    {
        public CompanyContactProfile()
        {
            CreateMap<CompanyContact, CompanyContactMapper>()
                    .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.PhoneNumber, dst => dst.MapFrom(op => op.PhoneNumber))
                    .ForMember(src => src.CompanyId, dst => dst.MapFrom(op => op.CompanyId))
                    .ForMember(src => src.Email, dst => dst.MapFrom(op => op.Email))
                    .ForMember(src => src.Mobile, dst => dst.MapFrom(op => op.Mobile))
                    .ForMember(src => src.Designation, dst => dst.MapFrom(op => op.Designation));

            this.CreateMap<CompanyContact, CompanyContactCreateMapper>()
                .ReverseMap();
            CreateMap<CompanyContactCreateMapper, CompanyContact>(MemberList.None)
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.PhoneNumber, dst => dst.MapFrom(op => op.PhoneNumber))
                    .ForMember(src => src.Email, dst => dst.MapFrom(op => op.Email))
                    .ForMember(src => src.Mobile, dst => dst.MapFrom(op => op.Mobile))
                    .ForMember(src => src.Designation, dst => dst.MapFrom(op => op.Designation));
        }
    }
}
