﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class POSProfile : Profile
    {
        public POSProfile()
        {
            this.CreateMap<POS, POSMapper>(MemberList.None)
                .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                .ForMember(src => src.EstablishmentId, dst => dst.MapFrom(op => op.EstablishmentId))
                .ForMember(src => src.UserId, dst => dst.MapFrom(op => op.UserId))
                .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                .ForMember(src => src.IPadUDID, dst => dst.MapFrom(op => op.IPadUDID))
                .ForMember(src => src.PrinterId, dst => dst.MapFrom(op => op.PrinterId))
                .ForMember(src => src.User, dst => dst.MapFrom(op => op.User))
                .ForMember(src => src.Establishment, dst => dst.MapFrom(op => op.Establishment))
                .ForMember(src => src.PrinterType, dst => dst.MapFrom(op => op.PrinterType))


                ;
            this.CreateMap<POS, POSCreateMapper>().ReverseMap();
            CreateMap<POSCreateMapper, POS>(MemberList.None)
                .ForMember(src => src.UserId, dst => dst.MapFrom(op => op.UserId))
                .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                .ForMember(src => src.IPadUDID, dst => dst.MapFrom(op => op.IPadUDID))
                .ForMember(src => src.PrinterId, dst => dst.MapFrom(op => op.PrinterId))
                .ForMember(src => src.PrinterType, dst => dst.MapFrom(op => op.PrinterType));

        }
    }
}
