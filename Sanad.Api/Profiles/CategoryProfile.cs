﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            this.CreateMap<Category, CategoryMapper>()
                .ReverseMap();

            this.CreateMap<Category, CategoryCreateMapper>()
                .ReverseMap();

            CreateMap<CategoryCreateMapper, Category > (MemberList.None)
             .ForMember(src => src.ArabicName,dst => dst.MapFrom(op => op.ArabicName))
              .ForMember(src => src.EnglishName, dst => dst.MapFrom(op => op.EnglishName));

            CreateMap<Category, CategoryMapper>()
             .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
             .ForMember(src => src.ArabicName, dst => dst.MapFrom(op => op.ArabicName))
              .ForMember(src => src.EnglishName, dst => dst.MapFrom(op => op.EnglishName));


        }
    }
}
