﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;

namespace Sanad.Api.Profiles
{
    public class SystemConfigurationProfile : Profile
    {
        public SystemConfigurationProfile()
        {
            CreateMap<SystemConfiguration, CreateEditSystemConfigurationMapper>(MemberList.None);
            CreateMap<CreateEditSystemConfigurationMapper, SystemConfiguration>(MemberList.None)
              .ForMember(src => src.NameEn, dst => dst.MapFrom(op => op.NameEn))
              .ForMember(src => src.NameAr, dst => dst.MapFrom(op => op.NameAr))
              .ForMember(src => src.ParentId, dst => dst.MapFrom(op => op.ParentId))
              .ForMember(src => src.Code, dst => dst.MapFrom(op => op.Code))

                ;
            CreateMap<SystemConfiguration, SystemConfigurationMapper>(MemberList.None)
                  .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                  .ForMember(src => src.NameEn, dst => dst.MapFrom(op => op.NameEn))
                  .ForMember(src => src.NameAr, dst => dst.MapFrom(op => op.NameAr))
                  .ForMember(src => src.ParentId, dst => dst.MapFrom(op => op.ParentId))
                  .ForMember(src => src.Code, dst => dst.MapFrom(op => op.Code))
                  .ForMember(src => src.Children, dst => dst.MapFrom(op => op.Children))
              ;
            CreateMap<SystemConfiguration, UserSystemConfigurationMapper>(MemberList.None)
             .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
             .ForMember(src => src.ParentId, dst => dst.MapFrom(op => op.ParentId))
             .ForMember(src => src.Code, dst => dst.MapFrom(op => op.Code))
             .ForMember(src => src.Children, dst => dst.MapFrom(op => op.Children))
         ;
            CreateMap<SystemConfigurationMapper, SystemConfiguration>(MemberList.None);


        }

    }
}
