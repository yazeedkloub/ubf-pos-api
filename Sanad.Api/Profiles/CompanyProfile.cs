﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class CompanyProfile : Profile
    {
        public CompanyProfile()
        {
            CreateMap<Company, CompanyMapper>(MemberList.None)
                    .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.City, dst => dst.MapFrom(op => op.City))
                    .ForMember(src => src.State, dst => dst.MapFrom(op => op.State))
                    .ForMember(src => src.Longitude, dst => dst.MapFrom(op => op.Longitude))
                    .ForMember(src => src.Latitude, dst => dst.MapFrom(op => op.Latitude))
                    .ForMember(src => src.Country, dst => dst.MapFrom(op => op.Country))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.Building, dst => dst.MapFrom(op => op.Building))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.TRN, dst => dst.MapFrom(op => op.TRN))
                    .ForMember(src => src.CompanyContacts, dst => dst.MapFrom(op => op.CompanyContacts));

            this.CreateMap<Company, CompanyCreateMapper>()
                .ReverseMap();
            CreateMap<CompanyCreateMapper, Company > (MemberList.None)
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.City, dst => dst.MapFrom(op => op.City))
                    .ForMember(src => src.State, dst => dst.MapFrom(op => op.State))
                    .ForMember(src => src.Longitude, dst => dst.MapFrom(op => op.Longitude))
                    .ForMember(src => src.Latitude, dst => dst.MapFrom(op => op.Latitude))
                    .ForMember(src => src.Country, dst => dst.MapFrom(op => op.Country))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.Building, dst => dst.MapFrom(op => op.Building))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.TRN, dst => dst.MapFrom(op => op.TRN));

            CreateMap<CompanyMapper, Company>();
        }
    }
}
