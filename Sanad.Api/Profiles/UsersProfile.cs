﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<User, UserMapper>()
                .ForMember(src => src.Username, dst => dst.MapFrom(op => op.Username))
                .ForMember(src => src.FirstName, dst => dst.MapFrom(op => op.FirstName))
                .ForMember(src => src.Permissions, dst => dst.MapFrom(op => op.Permissions))
                .ForMember(src => src.Companies, dst => dst.MapFrom(op => op.Companies))
                .ForMember(src => src.LastName, dst => dst.MapFrom(op => op.LastName));


            this.CreateMap<User, UserModelForCheck>()
                .ReverseMap();
            CreateMap<UserCreateMapper, User>(MemberList.None)
              .ForMember(src => src.Username, dst => dst.MapFrom(op => op.Username))
              .ForMember(src => src.FirstName, dst => dst.MapFrom(op => op.FirstName))
              .ForMember(src => src.Permissions, dst => dst.MapFrom(op => op.Permissions))
              .ForMember(src => src.Companies, dst => dst.MapFrom(op => op.Companies))
              .ForMember(src => src.LastName, dst => dst.MapFrom(op => op.LastName));
                   CreateMap<User, UserCreateMapper > (MemberList.None);
        }
    }
}
