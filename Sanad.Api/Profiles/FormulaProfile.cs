﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using Sanad.Services.ServiceResult;

namespace Sanad.Api.Profiles
{
    public class FormulaProfile :Profile
    {
        public FormulaProfile()
        {
            CreateMap<FilterResult<Item>, FormulaResultModelMapper>()
             .ForMember(src => src.Count, dst => dst.MapFrom(op => op.Count))
             .ForMember(src => src.Result, dst => dst.MapFrom(op => op.Result));

             ;

        }
       

    }
}
