﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class SalesProfile : Profile
    {
        public SalesProfile()
        {
            this.CreateMap<Sales, SalesMapper>().ReverseMap();
            

            this.CreateMap<Sales, SalesCreateMapper>()
                .ReverseMap();


            CreateMap<Sales, SalesMapper>()
                              .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                              .ForMember(src => src.POSId, dst => dst.MapFrom(op => op.POSId))
                              .ForMember(src => src.Comment, dst => dst.MapFrom(op => op.Comment))
                              .ForMember(src => src.DiscountAmount, dst => dst.MapFrom(op => op.DiscountAmount))
                              .ForMember(src => src.DiscountPercentage, dst => dst.MapFrom(op => op.DiscountPercentage))
                              .ForMember(src => src.DiscountReason, dst => dst.MapFrom(op => op.DiscountReason))
                              .ForMember(src => src.TotalQuantity, dst => dst.MapFrom(op => op.TotalQuantity))
                              .ForMember(src => src.TotalItems, dst => dst.MapFrom(op => op.TotalItems))
                              .ForMember(src => src.CartItems, dst => dst.MapFrom(op => op.CartItems))
                              .ForMember(src => src.Payments, dst => dst.MapFrom(op => op.Payments))
                              .ForMember(src => src.CustomerBarcode, dst => dst.MapFrom(op => op.CustomerBarcode))
                              .ForMember(src => src.CustomerName, dst => dst.MapFrom(op => op.CustomerName))
                              .ForMember(src => src.CustomerTRN, dst => dst.MapFrom(op => op.CustomerTRN))
                              .ForMember(src => src.TransactionId, dst => dst.MapFrom(op => op.TransactionId))
                              .ForMember(src => src.Subtotal, dst => dst.MapFrom(op => op.Subtotal))
                              .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax))
                              .ForMember(src => src.TotalAmount, dst => dst.MapFrom(op => op.TotalAmount))
                              .ForMember(src => src.Status, dst => dst.MapFrom(op => op.Status))
                              .ForMember(src => src.QuotationNumber, dst => dst.MapFrom(op => op.QuotationNumber))
                              .ForMember(src => src.UpdatedAt, dst => dst.MapFrom(op => op.UpdatedAt));

            CreateMap<SalesCreateMapper, Sales > ()
                              .ForMember(src => src.POSId, dst => dst.MapFrom(op => op.POSId))
                              .ForMember(src => src.Comment, dst => dst.MapFrom(op => op.Comment))
                              .ForMember(src => src.DiscountAmount, dst => dst.MapFrom(op => op.DiscountAmount))
                              .ForMember(src => src.DiscountPercentage, dst => dst.MapFrom(op => op.DiscountPercentage))
                              .ForMember(src => src.DiscountReason, dst => dst.MapFrom(op => op.DiscountReason))
                              .ForMember(src => src.TotalQuantity, dst => dst.MapFrom(op => op.TotalQuantity))
                              .ForMember(src => src.TotalItems, dst => dst.MapFrom(op => op.TotalItems))
                              .ForMember(src => src.CartItems, dst => dst.MapFrom(op => op.CartItems))
                              .ForMember(src => src.Payments, dst => dst.MapFrom(op => op.Payments))
                              .ForMember(src => src.CustomerBarcode, dst => dst.MapFrom(op => op.CustomerBarcode))
                              .ForMember(src => src.CustomerName, dst => dst.MapFrom(op => op.CustomerName))
                              .ForMember(src => src.CustomerTRN, dst => dst.MapFrom(op => op.CustomerTRN))
                              .ForMember(src => src.TransactionId, dst => dst.MapFrom(op => op.TransactionId))
                              .ForMember(src => src.Subtotal, dst => dst.MapFrom(op => op.Subtotal))
                              .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax))
                              .ForMember(src => src.TotalAmount, dst => dst.MapFrom(op => op.TotalAmount))
                              .ForMember(src => src.Status, dst => dst.MapFrom(op => op.Status))
                              .ForMember(src => src.QuotationNumber, dst => dst.MapFrom(op => op.QuotationNumber))
                              .ForMember(src => src.CreatedAt, dst => dst.MapFrom(op => op.CreatedAt));


        }
    }
}
