﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class InventoryProfile : Profile
    {
        public InventoryProfile()
        {
            this.CreateMap<Inventory, InventoryMapper>().ReverseMap();
            

            //this.CreateMap<Sales, SalesCreateMapper>()
            //    .ReverseMap();
            

        }
    }
}
