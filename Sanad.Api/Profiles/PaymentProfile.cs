﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;

namespace Sanad.Api.Profiles
{
    public class PaymentProfile : Profile
    {
        public PaymentProfile()
        {

            CreateMap<Payments, PaymentMapper > (MemberList.None)
                     .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                     .ForMember(src => src.PaymentType, dst => dst.MapFrom(op => op.PaymentType))
                     .ForMember(src => src.PaymentAmount, dst => dst.MapFrom(op => op.PaymentAmount));


            CreateMap<PaymentCreateMapper, Payments>(MemberList.None)
                .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                .ForMember(src => src.PaymentType, dst => dst.MapFrom(op => op.PaymentType))
                .ForMember(src => src.PaymentAmount, dst => dst.MapFrom(op => op.PaymentAmount));
        }

    }
}
