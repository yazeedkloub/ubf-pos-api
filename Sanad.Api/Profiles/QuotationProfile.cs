﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class QuotationProfile : Profile
    {
        public QuotationProfile()
        {
            this.CreateMap<Quotation, QuotationMapper>().ReverseMap();
            

            this.CreateMap<Quotation, QuotationCreateMapper>()
                .ReverseMap();


            CreateMap<Quotation, QuotationMapper>()
                              .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                              .ForMember(src => src.POSId, dst => dst.MapFrom(op => op.POSId))
                              .ForMember(src => src.Comment, dst => dst.MapFrom(op => op.Comment))
                              .ForMember(src => src.DiscountAmount, dst => dst.MapFrom(op => op.DiscountAmount))
                              .ForMember(src => src.DiscountPercentage, dst => dst.MapFrom(op => op.DiscountPercentage))
                              .ForMember(src => src.DiscountReason, dst => dst.MapFrom(op => op.DiscountReason))
                              .ForMember(src => src.TotalQuantity, dst => dst.MapFrom(op => op.TotalQuantity))
                              .ForMember(src => src.TotalItems, dst => dst.MapFrom(op => op.TotalItems))
                              .ForMember(src => src.CartItems, dst => dst.MapFrom(op => op.QuotationItems))
                              .ForMember(src => src.CustomerBarcode, dst => dst.MapFrom(op => op.CustomerBarcode))
                              .ForMember(src => src.CustomerName, dst => dst.MapFrom(op => op.CustomerName))
                              .ForMember(src => src.CustomerTRN, dst => dst.MapFrom(op => op.CustomerTRN))
                              .ForMember(src => src.TransactionId, dst => dst.MapFrom(op => op.TransactionId))
                              .ForMember(src => src.Subtotal, dst => dst.MapFrom(op => op.Subtotal))
                              .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax))
                              .ForMember(src => src.TotalAmount, dst => dst.MapFrom(op => op.TotalAmount))
                              .ForMember(src => src.UpdatedAt, dst => dst.MapFrom(op => op.UpdatedAt));

            CreateMap<QuotationCreateMapper, Quotation> ()
                              .ForMember(src => src.POSId, dst => dst.MapFrom(op => op.POSId))
                              .ForMember(src => src.Comment, dst => dst.MapFrom(op => op.Comment))
                              .ForMember(src => src.DiscountAmount, dst => dst.MapFrom(op => op.DiscountAmount))
                              .ForMember(src => src.DiscountPercentage, dst => dst.MapFrom(op => op.DiscountPercentage))
                              .ForMember(src => src.DiscountReason, dst => dst.MapFrom(op => op.DiscountReason))
                              .ForMember(src => src.TotalQuantity, dst => dst.MapFrom(op => op.TotalQuantity))
                              .ForMember(src => src.TotalItems, dst => dst.MapFrom(op => op.TotalItems))
                              .ForMember(src => src.QuotationItems, dst => dst.MapFrom(op => op.CartItems))
                              .ForMember(src => src.CustomerBarcode, dst => dst.MapFrom(op => op.CustomerBarcode))
                              .ForMember(src => src.CustomerName, dst => dst.MapFrom(op => op.CustomerName))
                              .ForMember(src => src.CustomerTRN, dst => dst.MapFrom(op => op.CustomerTRN))
                              .ForMember(src => src.TransactionId, dst => dst.MapFrom(op => op.TransactionId))
                              .ForMember(src => src.Subtotal, dst => dst.MapFrom(op => op.Subtotal))
                              .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax))
                              .ForMember(src => src.TotalAmount, dst => dst.MapFrom(op => op.TotalAmount))
                              .ForMember(src => src.CreatedAt, dst => dst.MapFrom(op => op.CreatedAt));


        }
    }
}
