﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class EstablishmentProfile : Profile
    {
        public EstablishmentProfile()
        {
            CreateMap<Establishment, EstablishmentMapper>(MemberList.None)
                    .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                    .ForMember(src => src.CompanyId, dst => dst.MapFrom(op => op.CompanyId))
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.Address, dst => dst.MapFrom(op => op.Address))
                    .ForMember(src => src.City, dst => dst.MapFrom(op => op.City))
                    .ForMember(src => src.State, dst => dst.MapFrom(op => op.State))
                    .ForMember(src => src.Longitude, dst => dst.MapFrom(op => op.Longitude))
                    .ForMember(src => src.Latitude, dst => dst.MapFrom(op => op.Latitude))
                    .ForMember(src => src.Country, dst => dst.MapFrom(op => op.Country))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.Building, dst => dst.MapFrom(op => op.Building))
                    .ForMember(src => src.EstablishmentContacts, dst => dst.MapFrom(op => op.EstablishmentContacts))
                    .ForMember(src => src.TaxType, dst => dst.MapFrom(op => op.TaxType))
                    .ForMember(src => src.Taxable, dst => dst.MapFrom(op => op.Taxable))
                    .ForMember(src => src.Logo, dst => dst.MapFrom(op => op.Logo))
                    .ForMember(src => src.Currency, dst => dst.MapFrom(op => op.Currency))
                    .ForMember(src => src.Company, dst => dst.MapFrom(op => op.Company))
                    .ForMember(src => src.HasVariableItem, dst => dst.MapFrom(op => op.HasVariableItem))
                    .ForMember(src => src.ColorCode, dst => dst.MapFrom(op => op.ColorCode))


                    ;
            this.CreateMap<Establishment, EstablishmentCreateMapper>()
                .ReverseMap();
            CreateMap<EstablishmentCreateMapper, Establishment>(MemberList.None)
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.Address, dst => dst.MapFrom(op => op.Address))
                    .ForMember(src => src.City, dst => dst.MapFrom(op => op.City))
                    .ForMember(src => src.State, dst => dst.MapFrom(op => op.State))
                    .ForMember(src => src.Longitude, dst => dst.MapFrom(op => op.Longitude))
                    .ForMember(src => src.Latitude, dst => dst.MapFrom(op => op.Latitude))
                    .ForMember(src => src.Country, dst => dst.MapFrom(op => op.Country))
                    .ForMember(src => src.Street, dst => dst.MapFrom(op => op.Street))
                    .ForMember(src => src.Building, dst => dst.MapFrom(op => op.Building))
                    .ForMember(src => src.TaxType, dst => dst.MapFrom(op => op.TaxType))
                    .ForMember(src => src.Taxable, dst => dst.MapFrom(op => op.Taxable))
                    .ForMember(src => src.Logo, dst => dst.MapFrom(op => op.Logo))
                    .ForMember(src => src.Currency, dst => dst.MapFrom(op => op.Currency))
                    .ForMember(src => src.HasVariableItem, dst => dst.MapFrom(op => op.HasVariableItem))
                    .ForMember(src => src.ColorCode, dst => dst.MapFrom(op => op.ColorCode));
        }
    }
}
