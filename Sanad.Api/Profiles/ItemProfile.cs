﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanad.Api.Profiles
{
    public class ItemProfile : Profile
    {
        public ItemProfile()
        {
            CreateMap<Item, Item>()
                .ForMember(dst => dst.Id, src => src.Ignore())
                .ForMember(dst => dst.Category, src => src.Ignore())
                .ForMember(dst => dst.Establishment, src => src.Ignore());
            CreateMap<Item, ItemsMapper>(MemberList.None)
                    .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                    .ForMember(src => src.EstablishmentId, dst => dst.MapFrom(op => op.EstablishmentId))
                    .ForMember(src => src.CategoryId, dst => dst.MapFrom(op => op.CategoryId))
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.Description, dst => dst.MapFrom(op => op.Description))
                    .ForMember(src => src.ItemNumber, dst => dst.MapFrom(op => op.ItemNumber))
                    .ForMember(src => src.BarcodeName, dst => dst.MapFrom(op => op.BarcodeName))
                    .ForMember(src => src.Price, dst => dst.MapFrom(op => op.Price))
                    .ForMember(src => src.Discount, dst => dst.MapFrom(op => op.Discount))
                    .ForMember(src => src.Quantity, dst => dst.MapFrom(op => op.Quantity))
                    .ForMember(src => src.Category, dst => dst.MapFrom(op => op.Category))
                    .ForMember(src => src.ItemId, dst => dst.MapFrom(op => op.ItemId));
            this.CreateMap<Item, ItemsCreateMapper>()
                .ReverseMap();
            CreateMap<ItemsCreateMapper, Item>(MemberList.None)
                    .ForMember(src => src.CategoryId, dst => dst.MapFrom(op => op.CategoryId))
                    .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                    .ForMember(src => src.Description, dst => dst.MapFrom(op => op.Description))
                    .ForMember(src => src.ItemNumber, dst => dst.MapFrom(op => op.ItemNumber))
                    .ForMember(src => src.BarcodeName, dst => dst.MapFrom(op => op.BarcodeName))
                    .ForMember(src => src.Price, dst => dst.MapFrom(op => op.Price))
                    .ForMember(src => src.Discount, dst => dst.MapFrom(op => op.Discount))
                    .ForMember(src => src.ItemId, dst => dst.MapFrom(op => op.ItemId))
                    .ForMember(src => src.CreatedAt, dst => dst.MapFrom(op => op.CreatedAt));

        }
    }
}
