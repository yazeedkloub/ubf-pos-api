﻿using AutoMapper;
using Sanad.Entities;
using Sanad.Models;

namespace Sanad.Api.Profiles
{
    public class QuotationItemProfile : Profile
    {
        public QuotationItemProfile()
        {

            CreateMap<QuotationItem, QuotationItemMapper > (MemberList.None)
                        .ForMember(src => src.Id, dst => dst.MapFrom(op => op.Id))
                        .ForMember(src => src.ItemId, dst => dst.MapFrom(op => op.ItemId))
                        .ForMember(src => src.CostPrice, dst => dst.MapFrom(op => op.CostPrice))
                        .ForMember(src => src.Description, dst => dst.MapFrom(op => op.Description))
                        .ForMember(src => src.Discount, dst => dst.MapFrom(op => op.Discount))
                        .ForMember(src => src.UnitPrice, dst => dst.MapFrom(op => op.UnitPrice))
                        .ForMember(src => src.ItemNumber, dst => dst.MapFrom(op => op.ItemNumber))
                        .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                        .ForMember(src => src.Quantity, dst => dst.MapFrom(op => op.Quantity))
                        .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax));


            CreateMap<QuotationItemCreateMapper, QuotationItem>(MemberList.None)
                              .ForMember(src => src.ItemId, dst => dst.MapFrom(op => op.ItemId))
                              .ForMember(src => src.CostPrice, dst => dst.MapFrom(op => op.CostPrice))
                              .ForMember(src => src.Description, dst => dst.MapFrom(op => op.Description))
                              .ForMember(src => src.Discount, dst => dst.MapFrom(op => op.Discount))
                              .ForMember(src => src.UnitPrice, dst => dst.MapFrom(op => op.UnitPrice))
                              .ForMember(src => src.ItemNumber, dst => dst.MapFrom(op => op.ItemNumber))
                              .ForMember(src => src.Name, dst => dst.MapFrom(op => op.Name))
                              .ForMember(src => src.Quantity, dst => dst.MapFrom(op => op.Quantity))
                              .ForMember(src => src.Tax, dst => dst.MapFrom(op => op.Tax));

        }
    }
}
