﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sanad.Api.Exptions;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Sanad.Api.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public ErrorHandlerMiddleware(RequestDelegate next, ILogger<ErrorHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

     

        public async Task Invoke(HttpContext context)
        {
            try
            {

                await _next(context);

                FormatRequest(context.Request);



            }
            catch (Exception error)
            {
                await HandleExceptionAsync(context, error);
                _logger.LogError(error.Message);

            }
        }

        private async Task  FormatRequest(HttpRequest request)
        {
         
            try
            {

                string headers = request.Headers.ToString();



                // return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString} {bodyAsText}";
                var rsult = request.Scheme + "://" + request.Host + request.Path + " , Method :" + request.Method + "  Query params:" + request.QueryString.Value  + " Headers:" + headers;
                _logger.LogError(rsult);
            }catch (Exception error)
            {

            }


        }
        private static async Task HandleExceptionAsync(HttpContext context, Exception error)
        {
            var response = context.Response;
            response.ContentType = "application/json";
   
            string message = error.Message;

            switch (error)
            {
                case CoreException e:
                    response.StatusCode = (int)e.Code;
                    message = e.Message;
                    break;
                default:
                    // unhandled error
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    message = "Faild";
                    break;
            }

            //ToDo: Serilog
            //Log.Error(error, "Exception thrown by the User '{@Name}' at route '{@Route}'", context.User.Identity.Name, context.Request.GetDisplayUrl());
            var responseModel = new ObjectResult(message);
            responseModel.StatusCode = response.StatusCode;
            var result = JsonSerializer.Serialize(responseModel);
            await response.WriteAsync(result);
        }

      

    }


}
