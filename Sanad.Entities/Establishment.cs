﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Establishment
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        [Required(ErrorMessage = "Please Provide a Establishment Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Address { get; set; } //{object}
        public string City { get; set; }
        public string State { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }

        public bool Taxable { get; set; }

        public TaxType? TaxType { get; set; }

        public string Currency { get; set; }

        public string Logo { get; set; }

        public List<Item> Items { get; set; }
        public List<Category> Categories { get; set; }
        //public List<Prep> Preps { get; set; }
        //public List<Product> Products { get; set; } 
        public List<POS> POSs { get; set; }
        public List<User> Users { get; set; }
        public List<EstablishmentContact> EstablishmentContacts { get; set; }

        public Company  Company { get; set; }

        public bool HasVariableItem { get; set; }

        public string ColorCode { get; set; }

        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }
    }

    public enum TaxType
    {
        Inclusive = 1,
        Exclusive = 2
    }
}
