﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class POS
    {
        public int Id { get; set; }
        public int EstablishmentId { get; set; }

        public int? UserId { get; set; }
        [Required(ErrorMessage ="Please Provide a POS Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public List<Sales> sales { get; set; }

        public string IPadUDID { get; set; }

        public string PrinterId { get; set; }

        public PrinterType?  PrinterType { get; set; }

        public User User { get; set; }

        public Establishment Establishment { get; set; }

        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }
    }

    public enum PrinterType
    {
        Epson = 1,
        Star
    }
}
