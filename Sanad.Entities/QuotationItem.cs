﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class QuotationItem
    {
        public int Id { get; set; }
        public int QuotationId { get; set; }
        public int? ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public float UnitPrice { get; set; }
        public float CostPrice { get; set; }
        public float Discount { get; set; }
        public float Tax { get; set; }
        public Item Item { get; set; }

        public Quotation Quotation { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
