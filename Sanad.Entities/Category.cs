﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public int? EstablishmentId { get; set; }
        [Required(ErrorMessage ="Please Provide a Cateory Arabic Name")]
        [StringLength(250)]
        public string ArabicName { get; set; }
        [Required(ErrorMessage = "Please Provide a Cateory English Name")]
        [StringLength(250)]
        public string EnglishName { get; set; }
        public List<Item> items { get; set; }

        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }


    }
}
