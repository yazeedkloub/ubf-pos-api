﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Payments
    {
        public int Id { get; set; }
        [ForeignKey("FK_SALE_ID_Payments")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SalesId { get; set; }
        public string PaymentType { get; set; }
        public string PaymentAmount { get; set; }
        public DateTime PaymentDate { get; set; } = DateTime.Now;
    }
}
