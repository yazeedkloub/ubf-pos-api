﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class SystemConfiguration
    {

        public SystemConfiguration()
        {
            Children = new List<SystemConfiguration>();
        }
        [Key]
        public int Id { get; set; }

        [StringLength(250)]
        public string NameEn { get; set; }

        [StringLength(250)]
        public string NameAr { get; set; }

        public string Code { get; set; }



        public int? ParentId { get; set; }

        public SystemConfiguration  Parent { get; set; }

        [ForeignKey("ParentId")]
        public List<SystemConfiguration> Children { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public bool IsDeleted { get; set; } = false;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }
    }
}
