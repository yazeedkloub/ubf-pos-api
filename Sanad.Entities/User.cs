﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sanad.Entities
{
    public class User
    {
        public User()
        {
        }
        public int Id { get; set; }
        public int? EstablishmentId { get; set; }

        

        [Required]
        [StringLength(250)]
        public string Username { get; set; }
        [Required]
        [StringLength(250)]
        public string Password { get; set; }
  
        [Required]
        [StringLength(250)]
        public string FirstName { get; set; }
        [StringLength(250)]
        public string LastName { get; set; }
        public bool IsActive { get; set; } = true;
        public bool IsAdmin { get; set; }
        [Required]
        [StringLength(250)]
        public string Permissions { get; set; }

        public string Companies { get; set; }

        public POS POS { get; set; }



        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }



    }
}
