﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Inventory
    {
            public List<int> categoryId { get; set; }
            public List<string> categoryName { get; set; }
            public List<int> SaleCount { get; set; }
    }
}
