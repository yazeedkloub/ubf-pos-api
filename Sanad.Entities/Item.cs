﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public int? EstablishmentId { get; set; }

        public int? CategoryId { get; set; }

        [Required(ErrorMessage = "Please Provide a Item Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public string BarcodeName { get; set; }
        public Category Category { get; set; }
        public float Price { get; set; } // from FixedCost to FixedCost 24112021
        public float Discount { get; set; } // Added 24112021
        public bool Yeild { get; set; }
        public bool NotTracked { get; set; } // Added 24112021
        public float YeildPercentage {get; set;}
        public float Quantity { get; set; }
     
        //[ForeignKey("FK_Items_ID_QuinatityUnit")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)] 
        public int? QuantityUnitId { get; set; }


        public string ItemId { get; set; }

        public Establishment Establishment { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }
    }
}
