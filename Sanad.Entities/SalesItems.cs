﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class SalesItems
    {
        public int Id { get; set; }
        [ForeignKey("FK_SALE_ID_SaleItems")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SalesId { get; set; }
        public int? ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public float UnitPrice { get; set; }
        public float CostPrice { get; set; }
        public float Discount { get; set; }
        public float Tax { get; set; }
        public Item Item { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        


    }

  
}
