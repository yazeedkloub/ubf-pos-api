﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Company
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Please Provide a Comapny Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string TRN { get; set; } //changed from taxId
        public List<Establishment> Establishments { get; set; }
        public List<CompanyContact> CompanyContacts { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }
    }
}
