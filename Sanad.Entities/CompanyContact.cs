﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class CompanyContact
    {
        public int Id { get; set; }
        //[ForeignKey("FK_COMAPNY_ID_CompanyContact")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyId { get; set; }
        [Required(ErrorMessage = "Please Provide a Contact Name")]
        [StringLength(250)]
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Designation { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
