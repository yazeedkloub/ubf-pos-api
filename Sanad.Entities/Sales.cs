﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Sales
    {
        public Sales()
        {
            CartItems = new List<SalesItems>();
            Payments = new List<Payments>();
        }
        public int Id { get; set; }
        public int POSId { get; set; }
        public int? UserId { get; set; }
        public string Comment { get; set; }
        public float DiscountAmount { get; set; }
        public float DiscountPercentage { get; set; }
        public string DiscountReason { get; set; }
        public float TotalQuantity { get; set; }
        public float TotalItems { get; set; }
        public List<SalesItems> CartItems { get; set; }
        public List<Payments> Payments { get; set; }
        public string CustomerBarcode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTRN { get; set; }
        public string TransactionId { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float TotalAmount { get; set; }
        public Status Status { get; set; }

        public int? QuotationNumber { get; set; }

        [ForeignKey("POSId")]
        public POS POS { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }
    }

    public enum Status
    {
        Suspend,
        Voided,
        Paid
    }
}
