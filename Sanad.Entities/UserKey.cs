﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class UserKey
    {
        public int  Id { get; set; }

        public int UserId { get; set; }

        public string Key { get; set; }

        public User User { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }
}
