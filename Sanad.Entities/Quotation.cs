﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Entities
{
    public class Quotation
    {

        public Quotation()
        {
            QuotationItems = new List<QuotationItem>();
        }
        public int Id { get; set; }
        public int POSId { get; set; }
        public int? UserId { get; set; }

        public string Comment { get; set; }
        public float DiscountAmount { get; set; }
        public float DiscountPercentage { get; set; }
        public string DiscountReason { get; set; }
        public float TotalQuantity { get; set; }
        public float TotalItems { get; set; }

        public string CustomerBarcode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTRN { get; set; }
        public string TransactionId { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float TotalAmount { get; set; }
        
        [ForeignKey("POSId")]
        public POS POS { get; set; }

        public List<QuotationItem>  QuotationItems { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public int? CreatedBy { get; set; }

        public int? UpdatedBy { get; set; }

        public int? DeletedBy { get; set; }

    }
}
