﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class EstablishmentRepository : Repository<Establishment>,IEstablishmentRepository
    {

        private readonly SanadDBContext _db;

        public EstablishmentRepository(SanadDBContext db):base(db)
        {
            _db = db;
        }
        public void AddEstablishment(Establishment Establishment)
        {
            Establishment.IsDeleted = false;
            Establishment.CreatedAt = DateTime.Now;
            Establishment.UpdatedAt = DateTime.Now;
            _db.Add(Establishment);
        }

        public async Task AddRangeEstablishment(List<Establishment> list)
        {

            await _db.AddRangeAsync(list);
        }
        public async Task<Establishment[]> GetAllEstablishmentAsync(int CompanyId)
        {
            IQueryable<Establishment> query = _db.establishments.Include(e => e.EstablishmentContacts.Where(ec => ec.IsDeleted ==false)).Where(e => e.CompanyId == CompanyId && e.IsDeleted == false); 
            return await query.ToArrayAsync();
        }

        public async Task<Establishment> GetEstablishmentByIdAsync(int CompanyId, int id)
        {
            IQueryable<Establishment> query = _db.establishments.Include(e => e.EstablishmentContacts.Where(ec => ec.IsDeleted == false))
                  .Where(e => e.Id == id && e.CompanyId == CompanyId && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }
        public async Task<Establishment> GetEstablishmentByIdAsync( int id)
        {
            IQueryable<Establishment> query = _db.establishments.Include(e => e.EstablishmentContacts.Where(ec => ec.IsDeleted == false))
                  .Where(e => e.Id == id && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }
        public async Task<Establishment> GetEstablishmentByNameAsync(int CompanyId, string Name)
        {
            IQueryable<Establishment> query = _db.establishments
               .Where(e => e.Name == Name && e.CompanyId == CompanyId && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public void RemoveEstablishment(Establishment Establishment)
        {
            _db.establishments.Remove(Establishment);
        }

        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
