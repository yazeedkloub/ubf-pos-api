﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface IRepository<T> where T : class
    {
        Task<bool> Any(Expression<Func<T, bool>> expression);

        Task<T> First(Expression<Func<T, bool>> expression = null);

        Task<List<T>> GetWhere(Expression<Func<T, bool>> expression = null, bool withTrack = true);

        Task<int> Count(Expression<Func<T, bool>> expression = null);

        void Delete(int id);
        Task<int> SaveChanges();
    }
}
