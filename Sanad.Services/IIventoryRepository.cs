﻿using Sanad.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface IInventoryRepository
    {
        Task<Inventory> GetItemsCategory();
    }
}
