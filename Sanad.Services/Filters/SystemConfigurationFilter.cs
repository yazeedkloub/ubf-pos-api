﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.Filters
{
    public class SystemConfigurationFilter : BaseFilter
    {
        public bool? OnlyParent { get; set; }

        public bool? SortByName { get; set; }
    }
}
