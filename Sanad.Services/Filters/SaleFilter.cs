﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.Filters
{
    public class SaleFilter: BaseFilter
    {
        public int? POSId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Status?  Status { get; set; }

        public List<int> Ids { get; set; }
    }
}
