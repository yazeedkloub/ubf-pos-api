﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.Filters
{
    // Base Filter for filtering
    public class BaseFilter
    {
     
        //Page number
        public int Page { get; set; } = 1;

        //Public search
        public string SearchQuery { get; set; }

        //Database sort direction
        public SortDirection SortDirection { get; set; } = SortDirection.Asc;

        private int _perPage;

        //Element count per page
        public int PerPage
        {
            get => _perPage <= 0 ? 10 : _perPage;
            set => _perPage = value <= 0 ? 10 : value;
        }

        //Check page number
        public int RequestedPage() =>   Page > 0 ? Page : 1;
         

        //If it equals true then return all elements else use pagination
        public bool? GetAll { get; set; } = false;

        //Calculate skipped elements 
        public int ElementsToBeSkipedCount() =>  (RequestedPage() - 1) * PerPage;
         

    }

    public enum SortDirection
    {
        Asc = 1,
        Desc = 2
    }
}
