﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.Filters
{
    public class UserFilter : BaseFilter
    {

        public bool? SortByEmail { get; set; }

        public bool? SortByName { get; set; }

        public bool? HasPOS { get; set; }

    }
}
