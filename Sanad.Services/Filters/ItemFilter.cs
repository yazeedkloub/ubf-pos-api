﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.Filters
{
    public class ItemFilter : BaseFilter
    {

        public string IPadUDID { get; set; }

        public int? POSUserId { get; set; }

        public int? EstablishmentId { get; set; }

        public List<int> CompanyIds { get; set; }

        public bool? OnlyDiscount { get; set; }

        public List<int> Ids { get; set; }


    }
}
