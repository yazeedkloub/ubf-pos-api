﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface ILoginLogRepository :  IRepository<LoginLog>
    {
        public void Add(LoginLog loginLog);
    }
}
