﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class LoginLogRepository : Repository<LoginLog>, ILoginLogRepository
    {

        private readonly SanadDBContext _db;
        public LoginLogRepository(SanadDBContext db) : base(db)
        {
            _db = db;
        }

        public void Add(LoginLog loginLog)
        {
            loginLog.CreatedAt = DateTime.Now;
            loginLog.UpdatedAt = DateTime.Now;
           _db.LoginLogs.Add(loginLog);
        }
    }
}
