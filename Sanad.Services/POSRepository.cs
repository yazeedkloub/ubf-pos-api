﻿ 
using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class POSRepository : Repository<POS>, IPOSRepository
    {

        private readonly SanadDBContext _db;

        public POSRepository(SanadDBContext db):base(db)    
        {
            _db = db;
        }
        public void AddPOS(POS Item)
        {
            Item.IsDeleted = false;
            Item.CreatedAt = DateTime.Now;
            Item.UpdatedAt = DateTime.Now;
            _db.Add(Item);
        }
        public async Task<POS[]> GetAllPOSAsync(int EstablishmentId)
        {
            IQueryable<POS> query = _db.POS.Include(p => p.User).Include(e => e.Establishment).ThenInclude(e => e.Company).ThenInclude(c => c.CompanyContacts).Where(e => e.EstablishmentId == EstablishmentId && e.IsDeleted == false);
            return await query.ToArrayAsync();
        }
        public async Task<POS> GetPOSByIdAsync(int EstablishmentId, int id)
        {
            IQueryable<POS> query = _db.POS.Include(p => p.User).Include(e => e.Establishment).ThenInclude(e => e.Company).ThenInclude(c => c.CompanyContacts)
                  .Where(e => e.EstablishmentId == EstablishmentId && e.Id == id);
            return await query.FirstOrDefaultAsync();
        }
        public async Task<POS> GetPOSByNameAsync(int EstablishmentId, string name)
        {
            IQueryable<POS> query = _db.POS.Include(p => p.User).Include(e => e.Establishment).ThenInclude(e => e.Company).ThenInclude(c => c.CompanyContacts)
               .Where(e => e.EstablishmentId == EstablishmentId && e.Name == name && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<POS> GetPOSByUDIDAsync(string udid)
        {
            IQueryable<POS> query = _db.POS.Include(p => p.User).Include(e => e.Establishment).ThenInclude(e => e.Company).ThenInclude(c => c.CompanyContacts)
               .Where(e => e.IPadUDID  == udid && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }



        public void RemovePOS(POS item)
        {
            _db.POS.Remove(item);
        }
        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
