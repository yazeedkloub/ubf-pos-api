﻿using Sanad.Entities;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface IUserRepository : IRepository<User>
    {

        IEnumerable<User> GetAllUsers();
        Task<User[]> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(int id);
        Task<User> GetUserByUsernameAsync(string username);
        Task<User> GetUserByUserNameAndPassowrdAsync(string password,int rquestId);
        Task<User> CheckUser(string username, string password);
        void AddUser<T>(T user) where T : class;
        void RemoveUser<T>(T user) where T : class;
        Task<bool> SaveAsync();
        bool Save();

        Task<FilterResult<User>> Filter(UserFilter filter);
    }
}
