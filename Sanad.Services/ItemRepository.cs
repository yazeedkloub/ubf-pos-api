﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using Sanad.Models;
using Sanad.Services.Extentions;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class ItemRespository : Repository<Item>, IItemRepository
    {

        private readonly SanadDBContext _db;

        public ItemRespository(SanadDBContext db) : base(db)
        {
            _db = db;
        }
        public void AddItems(Item Item)
        {
            Item.IsDeleted = false;
            Item.UpdatedAt = DateTime.Now;
            _db.Add(Item);
        }
        //Get all items 
        public async Task<Item[]> GetAllItemsAsync(int EstablishmentId)
        {
            IQueryable<Item> query = _db.Items.Include(i => i.Category).Include(i => i.Category).Where(e => e.IsDeleted == false && e.EstablishmentId == EstablishmentId);
            return await query.ToArrayAsync();
        }
        //Get items by id 
        public async Task<Item> GetItemsByIdAsync(int EstablishmentId, int id)
        {
            IQueryable<Item> query = _db.Items.Include(i => i.Category).Include(i => i.Category)
                  .Where(e => e.Id == id && e.EstablishmentId == EstablishmentId && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }
        //Get items by id 
        public async Task<Item> GetItemsByIdAsync(int id)
        {
            IQueryable<Item> query = _db.Items.Include(i => i.Category)
                  .Where(e => e.Id == id && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }
        //Get item by name 
        public async Task<Item> GetItemByNameAsync(int? EstablishmentId, string name)
        {
            IQueryable<Item> query = _db.Items
               .Where(e => e.Name == name && e.IsDeleted == false);

            if (EstablishmentId.HasValue)
                query = query.Where(i => i.EstablishmentId == EstablishmentId);

            return await query.FirstOrDefaultAsync();
        }

        //Get items with condition
        public async Task<List<Item>> GetWhere(Expression<Func<Item, bool>> expression)
        {
            return await _db.Items.Include(i => i.Category).Where(expression).Where(i => i.IsDeleted == false).ToListAsync();
        }

        //Filter items
        public async Task<FilterResult<Item>> Filter(ItemFilter filter, bool withTrack = false)
        {
            var query = withTrack ? _db.Items.Include(i => i.Category).AsQueryable() : _db.Items.AsNoTracking().Include(i => i.Category).AsQueryable();

            query = query.Where(i => i.IsDeleted == false);

            if (filter.EstablishmentId.HasValue)
            {
                //If the establishment has variable item then include the variable item with the list
                if (_db.establishments.Any(e => e.Id == filter.EstablishmentId && e.HasVariableItem))
                {
                    query = query.Where(i => i.EstablishmentId == filter.EstablishmentId || i.Name.ToLower().Contains("variable item"));

                }
                else
                {
                    //Else except the variable item
                    query = query.Where(i => i.EstablishmentId == filter.EstablishmentId && !i.Name.ToLower().Contains("variable item"));

                }

            }
            else
            {
                //Except the variable item
                query = query.Where(i => !i.Name.ToLower().Contains("variable item"));

            }

            //Get by IPadUDID
            if (!string.IsNullOrEmpty(filter.IPadUDID))
                query = query.Where(i => i.Establishment.POSs.Any(p => p.IPadUDID == filter.IPadUDID));

            //Get by HasValue
            if (filter.POSUserId.HasValue)
                query = query.Where(i => i.Establishment.POSs.Any(p => p.UserId == filter.POSUserId));
            //Get by CompanyIds
            if (filter.CompanyIds != null && filter.CompanyIds.Any())
                query = query.Where(i => filter.CompanyIds.Contains(i.Establishment.CompanyId));

            //Get by Ids
            if (filter.Ids != null && filter.Ids.Any())
                query = query.Where(i => filter.Ids.Contains(i.Id));

            //Search in items
            if (!string.IsNullOrEmpty(filter.SearchQuery))
            {
                query = query.Where(i => (i.Name + i.BarcodeName).ToLower().Contains(filter.SearchQuery.ToLower()));
            }
            if (filter.OnlyDiscount is true)
                query = query.Where(i => i.Name.ToLower().Contains("discount") && !i.EstablishmentId.HasValue);
            else
            {
                query = query.Where(i => !i.Name.ToLower().Contains("discount"));
            }

            query = query.OrderByDescending(i => i.Name.ToLower().Contains("variable item"));

            return await query.Pagination(filter);

        }

        public  IQueryable<Item> FilterProjection(ItemFilter filter)
        {
            var query = _db.Items.AsNoTracking().Include(i => i.Category).AsQueryable();

            query = query.Where(i => i.IsDeleted == false);

            if (filter.EstablishmentId.HasValue)
            {
                //If the establishment has variable item then include the variable item with the list
                if (_db.establishments.Any(e => e.Id == filter.EstablishmentId && e.HasVariableItem))
                {
                    query = query.Where(i => i.EstablishmentId == filter.EstablishmentId || i.Name.ToLower().Contains("variable item"));

                }
                else
                {
                    //Else except the variable item
                    query = query.Where(i => i.EstablishmentId == filter.EstablishmentId && !i.Name.ToLower().Contains("variable item"));

                }

            }
            else
            {
                //Except the variable item
                query = query.Where(i => !i.Name.ToLower().Contains("variable item"));

            }

            //Get by IPadUDID
            if (!string.IsNullOrEmpty(filter.IPadUDID))
                query = query.Where(i => i.Establishment.POSs.Any(p => p.IPadUDID == filter.IPadUDID));

            //Get by HasValue
            if (filter.POSUserId.HasValue)
                query = query.Where(i => i.Establishment.POSs.Any(p => p.UserId == filter.POSUserId));
            //Get by CompanyIds
            if (filter.CompanyIds != null && filter.CompanyIds.Any())
                query = query.Where(i => filter.CompanyIds.Contains(i.Establishment.CompanyId));

            //Get by Ids
            if (filter.Ids != null && filter.Ids.Any())
                query = query.Where(i => filter.Ids.Contains(i.Id));

            //Search in items
            if (!string.IsNullOrEmpty(filter.SearchQuery))
            {
                query = query.Where(i => (i.Name + i.BarcodeName).ToLower().Contains(filter.SearchQuery.ToLower()));
            }
            if (filter.OnlyDiscount is true)
                query = query.Where(i => i.Name.ToLower().Contains("discount") && !i.EstablishmentId.HasValue);
            else
            {
                query = query.Where(i => !i.Name.ToLower().Contains("discount"));
            }

            query = query.OrderByDescending(i => i.Name.ToLower().Contains("variable item"));

            return  query.Projection(filter);

        }
        public void RemoveItems(Item item)
        {
            _db.Items.Remove(item);
        }
        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }

        //Update items discount
        public async Task<FilterResult<Item>> UpdateDiscount(FormulaModel formula)
        {

            var condition = "";
            float equalPrice = 0;
            float lessPrice = 0;
            float greaterPrice = 0;
            var update = "UPDATE [dbo].[Items] SET Discount = ";
            var expressionList = new List<Expression<Func<Item, bool>>>();
            expressionList.Add(i => i.IsDeleted == false);

            //Get discoutn value 
            var discountValue = formula.DiscountType == DiscountType.Fixed ? formula.Discount : formula.Discount / 100;

            //Check if equal is exists
            var equal = formula.Conditions?.Where(c => c.ConditionType == ConditionType.Equals)?.FirstOrDefault();

            if (equal != null)
            {

                //Set condition value
                equalPrice = equal.Value;
                condition = " Price " + GetOperat(equal.ConditionType) + " @equalPrice";
                expressionList.Add(GetOperatExp(equal.ConditionType, equal.Value));

            }
            else
            {
                //Get first less than or less than equals
                var less = formula.Conditions?.Where(c => c.ConditionType == ConditionType.LessThan || c.ConditionType == ConditionType.LessEquals)?.FirstOrDefault();
                if (less != null)
                {
                    //Generate less condition
                    var lessOp = GetOperat(less.ConditionType);
                    lessPrice = less.Value;
                    condition += " Price " + lessOp + " @lessPrice";

                    expressionList.Add(GetOperatExp(less.ConditionType, less.Value));
                }
                //Get the first greater than or greater than equals 
                var greater = formula.Conditions?.Where(c => c.ConditionType == ConditionType.GreaterThan || c.ConditionType == ConditionType.GreaterEquals)?.FirstOrDefault();
                if (greater != null)
                {
                    //Generate greater condition
                    var greaterOp = GetOperat(greater.ConditionType);
                    greaterPrice = greater.Value;
                    condition += (less != null ? " AND " : "") + " Price " + greaterOp + " @greaterPrice";
                    expressionList.Add(GetOperatExp(greater.ConditionType, greater.Value));

                }
            }

            var discountQuery = "";
            var checkPrice = "";
            //If discount type Percentage
            if (formula.DiscountType == DiscountType.Percentage)
            {
                //Calc discount
                discountQuery = " Price - (Price * @discountValue)";
                expressionList.Add(i => (i.Price - (i.Price * discountValue)) <= 0);
                //Condition for items match with the condtion but the price will be less or equal 0
                checkPrice = " WHERE Price >  (Price * @discountValue) AND  ";
            }
            else
            {
                discountQuery = " Price - @discountValue";
                //Condition for items match with the condtion but the price will be less or equal 0
                checkPrice = " WHERE Price >  @discountValue AND  ";

                expressionList.Add(i => (i.Price - discountValue) <= 0);
            }
            //Update discount
            update += discountQuery;// " CASE WHEN (" + discountQuery + ") > 0 THEN (" + discountQuery + ") ELSE 0 END ";

            //Set condtion
            update += checkPrice +  condition;

            //Set params
            var greaterPriceParam = new SqlParameter("@greaterPrice", greaterPrice);
            var lessPriceParam = new SqlParameter("@lessPrice", lessPrice);
            var equalPriceParam = new SqlParameter("@equalPrice", equalPrice);

            var discountValueParam = new SqlParameter("@discountValue", discountValue);

            expressionList.Add(i => !i.Name.ToLower().Contains("variable item"));

            //Get items if the price is less or equla 0
            var itemQuery = _db.Items.AsNoTracking().AsQueryable();
            foreach (var expression in expressionList)
            {
                itemQuery = itemQuery.Where(expression);
            }

            var itemList = await itemQuery.ToListAsync();

         
            var result = await _db.Database.ExecuteSqlRawAsync(update, new[] { greaterPriceParam, lessPriceParam, equalPriceParam, discountValueParam });
            return new FilterResult<Item>(itemList, result);
        }

        //Get Operator
        private string GetOperat(ConditionType op)
        {
            switch (op)
            {
                case ConditionType.Equals:
                    return "=";
                case ConditionType.GreaterEquals:
                    return ">=";
                case ConditionType.LessEquals:
                    return "<=";
                case ConditionType.GreaterThan:
                    return ">";
                case ConditionType.LessThan:
                    return "<";
                default:
                    return "";

            }
        }

        private Expression<Func<Item, bool>> GetOperatExp(ConditionType op, float value)
        {
            switch (op)
            {
                case ConditionType.Equals:
                    return i => i.Price == value;
                case ConditionType.GreaterEquals:
                    return i => i.Price >= value;
                case ConditionType.LessEquals:
                    return i => i.Price <= value;
                case ConditionType.GreaterThan:
                    return i => i.Price > value;
                case ConditionType.LessThan:
                    return i => i.Price < value;
                default:
                    return i => i != null;

            }
        }
    }
}
