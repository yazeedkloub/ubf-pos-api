﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface IUserKeyRepository:IRepository<UserKey>
    {
        Task<UserKey> Generate(string userName);
    }
}
