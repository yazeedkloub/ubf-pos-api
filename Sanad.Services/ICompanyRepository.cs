﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface ICompanyRepository : IRepository<Company>
    {
        Task<Company[]> GetAllCompanyAsync(List<string> ids = null);
        Task<Company> GetCompanyByIdAsync(int id);
        Task<Company> GetCompanyByNameAsync(string Name);
        Task AddRangeCompany(List<Company> list);
        void AddCompany(Company category);
        void RemoveCompany(Company category);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
