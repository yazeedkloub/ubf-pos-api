﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class CategoryRepository : Repository<Category>,ICategoryRepository
    {

        private readonly SanadDBContext _db;

        public CategoryRepository(SanadDBContext db):base(db)
        {
            _db = db;
        }
        public void AddCategory(Category Category)
        {
            Category.IsDeleted = false;
            Category.CreatedAt = DateTime.Now;
            Category.UpdatedAt = DateTime.Now;
            _db.Add(Category);
        }

        public async Task<Category[]> GetAllCategoryAsync()
        {
            IQueryable<Category> query = _db.Categories.Where(e =>  e.IsDeleted == false); 
            return await query.ToArrayAsync();
        }

        public async Task<Category> GetCategoryByIdAsync( int id)
        {
            IQueryable<Category> query = _db.Categories
                  .Where(e =>   e.Id == id && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<Category> GetCategoryByNameAsync(string ArabicName, string EnglishName)
        {
            IQueryable<Category> query = _db.Categories
               .Where(e =>  e.ArabicName == ArabicName && e.EnglishName == EnglishName && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public void RemoveCategory(Category category)
        {
            _db.Categories.Remove(category);
        }

        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
