﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class CompanyContactRepository : Repository<CompanyContact>,ICompanyContactRepository
    {

        private readonly SanadDBContext _db;

        public CompanyContactRepository(SanadDBContext db):base(db)
        {
            _db = db;
        }
        public void AddCompanyContact(CompanyContact CompanyContact)
        {
            CompanyContact.IsDeleted = false;
            CompanyContact.CreatedAt = DateTime.Now;
            CompanyContact.UpdatedAt = DateTime.Now;
            _db.Add(CompanyContact);
        }

        public async Task<CompanyContact[]> GetAllCompanyContactAsync(int CompanyId)
        {
            IQueryable<CompanyContact> query = _db.companyContacts.Where(e => e.CompanyId == CompanyId && e.IsDeleted == false); 
            return await query.ToArrayAsync();
        }

        public async Task<CompanyContact> GetCompanyContactByIdAsync(int CompanyId, int id)
        {
            IQueryable<CompanyContact> query = _db.companyContacts
                  .Where(e => e.CompanyId == CompanyId && e.Id == id && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<CompanyContact> GetCompanyContactByNameAsync(int CompanyId, string Name)
        {
            IQueryable<CompanyContact> query = _db.companyContacts
               .Where(e => e.CompanyId == CompanyId && e.Name == Name && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public void RemoveCompanyContact(CompanyContact CompanyContact)
        {
            _db.companyContacts.Remove(CompanyContact);
        }

        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
