﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sanad.Services.Filters;
namespace Sanad.Services
{
    public interface IQuotationRepository : IRepository<Quotation>
    {

        Task<List<Quotation>> GetAllQuotationsAsync(string start_date, string end_date);
        Task<Quotation> GetQuotationByIdAsync(int id);

        Task<List<Quotation>> Filter(QuotationFilter filter);
        Task<Quotation> GetQuotationLastId();
        void AddQuotation(Quotation quotation);
        void RemoveQuotation(Quotation quotation);

        void Update(Quotation quotation);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
