﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  Sanad.Services.Extentions;
using System.Linq.Expressions;
using System.Security.Cryptography;

namespace Sanad.Services
{
    public class UserRepository : Repository<User>,IUserRepository
    {
        private readonly SanadDBContext _context;

        public UserRepository(SanadDBContext context):base(context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _context.Users.Where(u => u.IsDeleted == false)
                .OrderBy(u => u.Id).ToList();
        }

        public async Task<User[]> GetAllUsersAsync()
        {
            IQueryable<User> query = _context.Users.Where(u => u.IsDeleted == false);
            return await query.ToArrayAsync();
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            IQueryable<User> query = _context.Users
                .Where(u => u.Id == id).Where(u => u.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            IQueryable<User> query = _context.Users.Where(u => u.IsDeleted == false)
                .Where(u => u.Username == username);
            return await query.FirstOrDefaultAsync();
        }

    
        public async Task<User> GetUserByUserNameAndPassowrdAsync( string password, int rquestId)
        {
           // User user = await GetUserByUsernameAsync(username);
            var key = await _context.UserKeys.Include(u => u.User).FirstOrDefaultAsync(u => u.Id == rquestId);
            var user = key?.User;
            if (user != null && key != null)
            {
                bool verified = ComputeSha256Hash(user.Password + key.Key).ToLower() ==    password.ToLower();
                if (verified)
                {
                    return user;
                }
                return null;
            }
            return null;
        }

       private static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public async Task<FilterResult<User>> Filter(UserFilter filter)
        {

            if (filter == null) return null;

            var query = _context.Users.AsQueryable();
             query = query.Where(u => u.IsDeleted ==false);

            //Public search
            if (!string.IsNullOrEmpty(filter.SearchQuery))
                query = query.Where(u => u.Username.Contains(filter.SearchQuery) || u.FirstName.Contains(filter.SearchQuery) || u.LastName.Contains(filter.SearchQuery));
            

            if(filter.HasPOS.HasValue)
                query = filter.HasPOS is true ?  query.Where(u => u.POS != null && u.POS.IsDeleted == false) : query.Where(u => u.POS == null || u.POS.IsDeleted == true);

            if (filter.SortByEmail is true)
                query = query.Order(u => u.Username, filter);

            if (filter.SortByName is true)
                query = query.Order(u => u.FirstName+u.LastName, filter);

            return await query.Pagination(filter);

        }

        public async Task<User> CheckUser(string username, string password)
        {
            IQueryable<User> query = _context.Users.Where(u => u.IsDeleted == false)
                .Where(u => u.Username == username && u.Password == password);
            return await query.FirstOrDefaultAsync();
        }

        public void AddUser<T>(T user) where T : class
        {
            _context.Add(user);
        }

        public void RemoveUser<T>(T user) where T : class
        {
            _context.Remove(user);
        }

        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public bool Save()
        {
            return (_context.SaveChanges() > 0);
        }

    }
}
