﻿using Sanad.Entities;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface ISystemConfigurationRepository : IRepository<SystemConfiguration>
    {
        IEnumerable<SystemConfiguration> GetAll();
        Task<List<SystemConfiguration>> GetAllAsync();
        Task<List<SystemConfiguration>> GetAllAsync(List<int> permissions);
        Task<SystemConfiguration> GetByIdAsync(int id);

        void Add<T>(T configuration) where T : class;
        void Remove(SystemConfiguration configuration);
        Task<bool> SaveAsync();
        bool Save();

        Task<FilterResult<SystemConfiguration>> Filter(SystemConfigurationFilter filter);

         SystemConfiguration GetChildren(SystemConfiguration systemConfiguration, List<int> permissions = null);




    }
}
