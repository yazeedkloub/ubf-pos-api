﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class UserKeyRepository : Repository<UserKey>, IUserKeyRepository
    {
        private readonly SanadDBContext _db;
        public UserKeyRepository(SanadDBContext db) : base(db)
        {
            _db = db;
        }


        public async Task<UserKey> Generate(string userName)
        {
          var user = await  _db.Users.FirstOrDefaultAsync(u => u.Username == userName);
         
            if (user != null)
            {
                var key = GetKey();

                while(_db.UserKeys.Any(u => u.Key == key))
                {
                    key = GetKey();
                }

                _db.UserKeys.Add(new UserKey()
                {
                    Key = key,
                    UserId = user.Id
                });
                await _db.SaveChangesAsync();

                return await _db.UserKeys.FirstOrDefaultAsync(u => u.Key == key);
                   
            }

            return null;
        }


        private string GetKey()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 30)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray()).ToLower();

            return result;
        }
    }
}
