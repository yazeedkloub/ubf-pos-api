﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public abstract class Repository<T>: IRepository<T> where T : class
    {
        private readonly SanadDBContext _db;

        public Repository(SanadDBContext db)
        {
            _db = db;
        }

        public async Task<bool> Any(Expression<Func<T, bool>> expression)
        {   
            return await _db.Set<T>().AnyAsync(expression);
        }

        public async Task<T> First(Expression<Func<T, bool>> expression = null) 
        {
            return await (expression == null ? _db.Set<T>().FirstOrDefaultAsync() : _db.Set<T>().FirstOrDefaultAsync(expression));
        }

        public async Task<List<T>> GetWhere(Expression<Func<T, bool>> expression = null,bool withTrack = true) 
        {
            var query = withTrack ? _db.Set<T>() : _db.Set<T>().AsNoTracking();
            return await (expression == null ? query.ToListAsync() : query.Where(expression).ToListAsync());
        }

        public async Task<int> Count(Expression<Func<T, bool>> expression = null)
        {
            return await (expression == null ? _db.Set<T>().CountAsync() : _db.Set<T>().CountAsync(expression));
        }

        public void Delete(int id)
        {
            var entity = _db.Set<T>().Find(id);
            if(entity != null)
            {
                _db.Set<T>().Remove(entity);
            }
        }
        public  async Task<int> SaveChanges()
        {
           
                return await _db.SaveChangesAsync();
         
        }

    }
}
