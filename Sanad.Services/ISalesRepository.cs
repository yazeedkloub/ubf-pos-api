﻿using Sanad.Entities;
using Sanad.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public interface ISalesRepository : IRepository<Sales>
    {
        Task<List<Sales>> GetAllSalesAsync(string start_date, string end_date);
        Task<Sales> GetSalesByIdAsync(int id);

        Task<List<Sales>> Filter(SaleFilter filter);
        Task<Sales> GetSaleLastId();
        void AddSales(Sales sale);
        void RemoveSales(Sales sale);

        void Update(Sales sale);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
