﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface ICategoryRepository : IRepository<Category>
    {
        Task<Category[]> GetAllCategoryAsync();
        Task<Category> GetCategoryByIdAsync( int id);
        Task<Category> GetCategoryByNameAsync(string ArabicName, string EnglishName);
        void AddCategory(Category category);
        void RemoveCategory(Category category);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
