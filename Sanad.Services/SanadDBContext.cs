﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;

namespace Sanad.Services
{
    public class SanadDBContext : DbContext
    {
        public SanadDBContext(DbContextOptions<SanadDBContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Company>().HasKey(c => c.Id);
            //modelBuilder.Entity<Company>().HasMany(c => c.Establishments);
            //modelBuilder.Entity<Company>().HasMany(c => c.CompanyContacts);

            //modelBuilder.Entity<Establishment>().HasKey(c => c.Id);
            //modelBuilder.Entity<Establishment>().HasMany(c => c.Items);
            //modelBuilder.Entity<Establishment>().HasMany(c => c.Categories);
            //modelBuilder.Entity<Establishment>().HasMany(c => c.POSs);
            //modelBuilder.Entity<Establishment>().HasMany(c => c.Users);


            //modelBuilder.Entity<Item>().HasKey(c => c.Id);            
            //modelBuilder.Entity<Item>().HasMany(c => c.PrepIngredients);
            //modelBuilder.Entity<Item>().HasMany(c => c.ProductIngredients);

            //modelBuilder.Entity<Prep>().HasKey(c => c.Id);
            //modelBuilder.Entity<Prep>().HasMany(c => c.prepIngredients);

            //modelBuilder.Entity<Product>().HasKey(c => c.Id);
            //modelBuilder.Entity<Product>().HasMany(c => c.ProductIngredients);
            ////modelBuilder.Entity<Prep>().HasForeignKey(p => p.BlogForeignKey);


            //modelBuilder.Entity<Sales>().HasKey(c => c.Id);
            //modelBuilder.Entity<Sales>().HasMany(c => c.CartItems);
            ////modelBuilder.Entity<SalesItems>().has
            ////modelBuilder.Entity<SalesItems>()
            ////    .HasOne(e => e.Sales)
            ////    .WithMany(c => c.salesItems);
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyContact> companyContacts { get; set; }
        public DbSet<Establishment> establishments { get; set; }
        public DbSet<EstablishmentContact> establishmentContacts { get; set; }
        public DbSet<Item> Items { get; set; }
 
        public DbSet<POS> POS { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<SalesItems> Sales_items { get; set; }
        public DbSet<Payments> Payments { get; set; }
        public DbSet<Category> Categories { get; set; }

        public DbSet<SystemConfiguration> SystemConfigurations { get; set; }

        public DbSet<UserKey> UserKeys { get; set; }

        public DbSet<LoginLog> LoginLogs { get; set; }

        public DbSet<Quotation> Quotations { get; set; }

        public DbSet<QuotationItem> QuotationItems { get; set; }







    }
}
