﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class CompanyRepository : Repository<Company>,ICompanyRepository
    {

        private readonly SanadDBContext _db;

        public CompanyRepository(SanadDBContext db):base(db)
        {
            _db = db;
        }
        public void AddCompany(Company Company)
        {
            Company.IsDeleted = false;
            Company.CreatedAt = DateTime.Now;
            Company.UpdatedAt = DateTime.Now;
            _db.Add(Company);
        }
        public async Task  AddRangeCompany(List<Company> list)
        {
           
          await  _db.AddRangeAsync(list);
        }
        public async Task<Company[]> GetAllCompanyAsync(List<string> ids = null)
        {
            IQueryable<Company> query = _db.Companies.Include(c => c.CompanyContacts.Where(cc => cc.IsDeleted == false)).Where(e => e.IsDeleted == false );

            if (ids != null && ids.Any())
            {
                var companysList = ids.Select(c => int.TryParse(c, out int id) ? id : 0).Where(c => c > 0).ToList();
                if(companysList.Count > 0)

                query = query.Where(c => companysList.Contains(c.Id));

            }
            return await query.ToArrayAsync();
        }

        public async Task<Company> GetCompanyByIdAsync(int id)
        {
            IQueryable<Company> query = _db.Companies.Include(c => c.CompanyContacts.Where(cc => cc.IsDeleted == false))
                  .Where(e => e.Id == id && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<Company> GetCompanyByNameAsync(string Name)
        {
            IQueryable<Company> query = _db.Companies
               .Where(e => e.Name == Name && e.IsDeleted == false);
            return await query.FirstOrDefaultAsync();
        }

        public void RemoveCompany(Company Company)
        {
            _db.Companies.Remove(Company);
        }

        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
