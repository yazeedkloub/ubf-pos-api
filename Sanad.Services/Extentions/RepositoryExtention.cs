﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;

namespace Sanad.Services.Extentions
{
    public static class RepositoryExtention
    {
        //Repository pagination use T as model and F as a filter extended from BaseFilter class 
        public static async Task<FilterResult<T>> Pagination<T,F>(this IQueryable<T> query,F filter,bool WitCount = false) where T:class where F:BaseFilter
        {

            int count =  WitCount ?  await query.CountAsync() : 0;

            //If GetAll equals true return all elemants else use pagination
            var result =await ( filter == null || filter.GetAll is true ? query.ToListAsync() : query.Skip(filter.ElementsToBeSkipedCount()).Take(filter.PerPage).ToListAsync());
            
            return new FilterResult<T>(result, count);
        }

        public static  IQueryable<T> Projection<T, F>(this IQueryable<T> query, F filter) where T : class where F : BaseFilter
        {

           

            //If GetAll equals true return all elemants else use pagination
             return  (filter == null || filter.GetAll is true ? query : query.Skip(filter.ElementsToBeSkipedCount()).Take(filter.PerPage));

        }

        //Repository order by
        public static IQueryable<T> Order<T, F,Tkey>(this IQueryable<T> query, Expression<Func<T, Tkey>> expression, F filter ) where T : class where F : BaseFilter
        {

        
            return filter.SortDirection == SortDirection.Desc ? query.OrderByDescending(expression) : query.OrderBy(expression);
        }
    }
}
