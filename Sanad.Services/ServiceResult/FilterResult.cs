﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services.ServiceResult
{
    //After filter 
    public class FilterResult<T>
    { 
    
        public FilterResult(List<T> result)
    {

            Result = result;
    }
        public FilterResult(List<T> result,int count)
        {

            Result = result;
            Count = count;
           
        }
        //Elements count
        public int Count { get; set; }

        //Element
        public List<T> Result { get; set; }
    }
}
