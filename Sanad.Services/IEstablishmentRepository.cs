﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface IEstablishmentRepository : IRepository<Establishment>
    {
        Task<Establishment[]> GetAllEstablishmentAsync(int CompanyId);
        Task<Establishment> GetEstablishmentByIdAsync(int CompanyId, int id);
        Task<Establishment> GetEstablishmentByNameAsync(int CompanyId, string Name);
        Task<Establishment> GetEstablishmentByIdAsync(int id);
        void AddEstablishment(Establishment category);
        void RemoveEstablishment(Establishment category);

        Task AddRangeEstablishment(List<Establishment> list);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
