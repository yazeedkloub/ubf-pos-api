﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface IEstablishmentContactRepository : IRepository<EstablishmentContact>
    {
        Task<EstablishmentContact[]> GetAllEstablishmentContactAsync(int EstablishmentId);
        Task<EstablishmentContact> GetEstablishmentContactByIdAsync(int EstablishmentId, int id);
        Task<EstablishmentContact> GetEstablishmentContactByNameAsync(int EstablishmentId, string Name);
        void AddEstablishmentContact(EstablishmentContact category);
        void RemoveEstablishmentContact(EstablishmentContact category);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
