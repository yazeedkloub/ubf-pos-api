﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using Sanad.Services.Extentions;
using Sanad.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class QuotationRepository :Repository<Quotation>,IQuotationRepository
    {
        private readonly SanadDBContext _db;
        public QuotationRepository(SanadDBContext db) : base(db)
        {
            _db = db;
        }

        public void AddQuotation(Quotation quotation)
        {
            
            
                quotation.IsDeleted = false;
                quotation.CreatedAt = DateTime.Now;
                quotation.UpdatedAt = DateTime.Now;
                _db.Quotations.Add(quotation);
           
        }
        public async Task<List<Quotation>> GetAllQuotationsAsync(string start_date, string end_date)
        {
            IQueryable<Quotation> query = _db.Quotations.Include(q => q.QuotationItems);
            List<Quotation> quotations = new List<Quotation>();
            DateTime startDate;// = DateTime.Now.AddDays(-30);
            DateTime endDate;// = DateTime.Now.AddDays(+30);
            if (start_date != "" && start_date != null && end_date != "" && end_date != null)
            {
                startDate = DateTime.Parse(start_date);
                endDate = DateTime.Parse(end_date);
                quotations = _db.Quotations.Where(a => a.CreatedAt.Date >= startDate && a.CreatedAt.Date <= endDate).ToList();
            }
            else
            {
                quotations = _db.Quotations.ToList();
            }

          
            return quotations;
        }

        public async Task<List<Quotation>> Filter(QuotationFilter filter)
        {
            var query = _db.Quotations.Include(s => s.QuotationItems).AsQueryable();
            query = query.Where(s => s.IsDeleted == false);
            if (filter.POSId.HasValue)
                query = query.Where(s => s.POSId == filter.POSId);

            if (filter.StartDate.HasValue)
                query = query.Where(s => s.CreatedAt >= filter.StartDate);

            if (filter.EndDate.HasValue)
                query = query.Where(s => s.CreatedAt >= filter.EndDate);

      

            if (filter?.Ids?.Any() is true)
                query = query.Where(s => filter.Ids.Contains(s.Id));

            query = query.OrderByDescending(s => s.UpdatedAt);

            var result = await query.Pagination(filter);
            return result.Result;

        }
        public async Task<Quotation> GetQuotationByIdAsync(int id)
        {

            Quotation quotation = new Quotation();
            quotation = await _db.Quotations.Include(q => q.QuotationItems).Where(e => e.Id == id).FirstOrDefaultAsync();
        
   
    
            return quotation;
        }
        public async Task<Quotation> GetQuotationLastId()
        {
            Quotation quotation = new Quotation();
            quotation = await _db.Quotations.Include(q => q.QuotationItems).OrderByDescending(p => p.Id).FirstOrDefaultAsync();
        
            return quotation;
        }



        public void Update(Quotation quotation)
        {
            if (quotation != null)
            {
                var itemIds = quotation.QuotationItems.Select(i => i.ItemId);
                var items = _db.QuotationItems.Where(i => i.QuotationId == quotation.Id && itemIds.Contains(i.ItemId)).ToList();
                _db.QuotationItems.RemoveRange(items);
                quotation.UpdatedAt = DateTime.Now;
              

            }
        }
        //
        public void RemoveQuotation(Quotation quotation)
        {
            _db.Quotations.Remove(quotation);
        }
        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}

