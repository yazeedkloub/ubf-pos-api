﻿using Sanad.Entities;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Sanad.Models;

namespace Sanad.Services
{
   public interface IItemRepository : IRepository<Item>
    {
        Task<Item[]> GetAllItemsAsync(int EstablishmentId);
        Task<Item> GetItemsByIdAsync(int EstablishmentId, int id);
        Task<Item> GetItemsByIdAsync( int id);

        Task<Item> GetItemByNameAsync(int? EstablishmentId, string name );
        Task<List<Item>> GetWhere(Expression<Func<Item, bool>> expression);

        Task<FilterResult<Item>> Filter(ItemFilter filter, bool withTrack = false);
        IQueryable<Item> FilterProjection(ItemFilter filter);
        Task<FilterResult<Item>> UpdateDiscount(FormulaModel formula);
        void AddItems(Item item);
        void RemoveItems(Item item);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
