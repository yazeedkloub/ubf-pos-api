﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface IPOSRepository : IRepository<POS>
    {
        Task<POS[]> GetAllPOSAsync(int EstablishmentId);
        Task<POS> GetPOSByIdAsync(int EstablishmentId, int id);
        void AddPOS(POS pos);
        Task<POS> GetPOSByNameAsync(int EstablishmentId, string name);

        Task<POS> GetPOSByUDIDAsync(string udid);
        void RemovePOS(POS pos);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
