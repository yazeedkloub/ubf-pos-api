﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class AddVariableItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FixedCost",
                table: "Items",
                newName: "Price");

            migrationBuilder.RenameColumn(
                name: "DynamicCost",
                table: "Items",
                newName: "Discount");

            migrationBuilder.AddColumn<bool>(
                name: "HasVariableItem",
                table: "establishments",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasVariableItem",
                table: "establishments");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "Items",
                newName: "FixedCost");

            migrationBuilder.RenameColumn(
                name: "Discount",
                table: "Items",
                newName: "DynamicCost");
        }
    }
}
