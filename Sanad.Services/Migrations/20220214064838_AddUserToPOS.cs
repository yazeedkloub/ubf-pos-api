﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class AddUserToPOS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "POSId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MacAdress",
                table: "POS",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrinterId",
                table: "POS",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "establishments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "establishments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TaxType",
                table: "establishments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Taxable",
                table: "establishments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Users_POSId",
                table: "Users",
                column: "POSId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_POS_POSId",
                table: "Users",
                column: "POSId",
                principalTable: "POS",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_POS_POSId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_POSId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "POSId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "MacAdress",
                table: "POS");

            migrationBuilder.DropColumn(
                name: "PrinterId",
                table: "POS");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "establishments");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "establishments");

            migrationBuilder.DropColumn(
                name: "TaxType",
                table: "establishments");

            migrationBuilder.DropColumn(
                name: "Taxable",
                table: "establishments");
        }
    }
}
