﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sanad.Services.Migrations
{
    public partial class UpdateCompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Phone_Number",
                table: "companyContacts",
                newName: "PhoneNumber");

            migrationBuilder.RenameColumn(
                name: "street",
                table: "Companies",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "state",
                table: "Companies",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "longitude",
                table: "Companies",
                newName: "Longitude");

            migrationBuilder.RenameColumn(
                name: "latitude",
                table: "Companies",
                newName: "Latitude");

            migrationBuilder.RenameColumn(
                name: "country",
                table: "Companies",
                newName: "Country");

            migrationBuilder.RenameColumn(
                name: "city",
                table: "Companies",
                newName: "City");

            migrationBuilder.RenameColumn(
                name: "building",
                table: "Companies",
                newName: "Building");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhoneNumber",
                table: "companyContacts",
                newName: "Phone_Number");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "Companies",
                newName: "street");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "Companies",
                newName: "state");

            migrationBuilder.RenameColumn(
                name: "Longitude",
                table: "Companies",
                newName: "longitude");

            migrationBuilder.RenameColumn(
                name: "Latitude",
                table: "Companies",
                newName: "latitude");

            migrationBuilder.RenameColumn(
                name: "Country",
                table: "Companies",
                newName: "country");

            migrationBuilder.RenameColumn(
                name: "City",
                table: "Companies",
                newName: "city");

            migrationBuilder.RenameColumn(
                name: "Building",
                table: "Companies",
                newName: "building");
        }
    }
}
