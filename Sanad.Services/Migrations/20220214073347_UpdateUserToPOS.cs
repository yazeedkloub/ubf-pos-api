﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class UpdateUserToPOS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_POS_POSId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_POSId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "POSId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "POS",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_POS_UserId",
                table: "POS",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_POS_Users_UserId",
                table: "POS",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_POS_Users_UserId",
                table: "POS");

            migrationBuilder.DropIndex(
                name: "IX_POS_UserId",
                table: "POS");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "POS");

            migrationBuilder.AddColumn<int>(
                name: "POSId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_POSId",
                table: "Users",
                column: "POSId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_POS_POSId",
                table: "Users",
                column: "POSId",
                principalTable: "POS",
                principalColumn: "Id");
        }
    }
}
