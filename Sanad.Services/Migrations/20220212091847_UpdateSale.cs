﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class UpdateSale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "item_id",
                table: "Sales_items");

            migrationBuilder.RenameColumn(
                name: "tax",
                table: "Sales_items",
                newName: "Tax");

            migrationBuilder.RenameColumn(
                name: "quantity",
                table: "Sales_items",
                newName: "Quantity");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Sales_items",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "discount",
                table: "Sales_items",
                newName: "Discount");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "Sales_items",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "unit_price",
                table: "Sales_items",
                newName: "UnitPrice");

            migrationBuilder.RenameColumn(
                name: "item_number",
                table: "Sales_items",
                newName: "ItemNumber");

            migrationBuilder.RenameColumn(
                name: "cost_price",
                table: "Sales_items",
                newName: "CostPrice");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "Sales_items",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Sales",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Sales",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy",
                table: "Sales",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Sales",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sales_items_ItemId",
                table: "Sales_items",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_items_Items_ItemId",
                table: "Sales_items",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sales_items_Items_ItemId",
                table: "Sales_items");

            migrationBuilder.DropIndex(
                name: "IX_Sales_items_ItemId",
                table: "Sales_items");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Sales_items");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Sales");

            migrationBuilder.RenameColumn(
                name: "Tax",
                table: "Sales_items",
                newName: "tax");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "Sales_items",
                newName: "quantity");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Sales_items",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "Discount",
                table: "Sales_items",
                newName: "discount");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Sales_items",
                newName: "description");

            migrationBuilder.RenameColumn(
                name: "UnitPrice",
                table: "Sales_items",
                newName: "unit_price");

            migrationBuilder.RenameColumn(
                name: "ItemNumber",
                table: "Sales_items",
                newName: "item_number");

            migrationBuilder.RenameColumn(
                name: "CostPrice",
                table: "Sales_items",
                newName: "cost_price");

            migrationBuilder.AddColumn<string>(
                name: "item_id",
                table: "Sales_items",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Sales",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
