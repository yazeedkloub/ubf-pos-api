﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class UpateSystemConfigurationName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "SystemConfigurations",
                newName: "NameEn");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "SystemConfigurations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameAr",
                table: "SystemConfigurations",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "SystemConfigurations");

            migrationBuilder.DropColumn(
                name: "NameAr",
                table: "SystemConfigurations");

            migrationBuilder.RenameColumn(
                name: "NameEn",
                table: "SystemConfigurations",
                newName: "Name");
        }
    }
}
