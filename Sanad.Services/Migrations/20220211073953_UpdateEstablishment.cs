﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sanad.Services.Migrations
{
    public partial class UpdateEstablishment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "street",
                table: "establishments",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "state",
                table: "establishments",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "longitude",
                table: "establishments",
                newName: "Longitude");

            migrationBuilder.RenameColumn(
                name: "latitude",
                table: "establishments",
                newName: "Latitude");

            migrationBuilder.RenameColumn(
                name: "country",
                table: "establishments",
                newName: "Country");

            migrationBuilder.RenameColumn(
                name: "city",
                table: "establishments",
                newName: "City");

            migrationBuilder.RenameColumn(
                name: "building",
                table: "establishments",
                newName: "Building");

            migrationBuilder.RenameColumn(
                name: "Phone_Number",
                table: "establishmentContacts",
                newName: "PhoneNumber");

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "establishments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy",
                table: "establishments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "establishments",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "establishments");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "establishments");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "establishments");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "establishments",
                newName: "street");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "establishments",
                newName: "state");

            migrationBuilder.RenameColumn(
                name: "Longitude",
                table: "establishments",
                newName: "longitude");

            migrationBuilder.RenameColumn(
                name: "Latitude",
                table: "establishments",
                newName: "latitude");

            migrationBuilder.RenameColumn(
                name: "Country",
                table: "establishments",
                newName: "country");

            migrationBuilder.RenameColumn(
                name: "City",
                table: "establishments",
                newName: "city");

            migrationBuilder.RenameColumn(
                name: "Building",
                table: "establishments",
                newName: "building");

            migrationBuilder.RenameColumn(
                name: "PhoneNumber",
                table: "establishmentContacts",
                newName: "Phone_Number");
        }
    }
}
