﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sanad.Services.Migrations
{
    public partial class RemoveQunatityUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_QuinatityUnits_QuantityUnitId",
                table: "Items");

            migrationBuilder.DropTable(
                name: "QuinatityUnits");

            migrationBuilder.DropIndex(
                name: "IX_Items_QuantityUnitId",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "QuantityUnitId",
                table: "Items",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "QuantityUnitId",
                table: "Items",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "QuinatityUnits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArabicName = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EnglishName = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuinatityUnits", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_QuantityUnitId",
                table: "Items",
                column: "QuantityUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_QuinatityUnits_QuantityUnitId",
                table: "Items",
                column: "QuantityUnitId",
                principalTable: "QuinatityUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
