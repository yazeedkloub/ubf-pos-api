﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class UpdatePOSAndItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_POS_UserId",
                table: "POS");

            migrationBuilder.RenameColumn(
                name: "MacAdress",
                table: "POS",
                newName: "IPadUDID");

            migrationBuilder.AddColumn<string>(
                name: "ItemId",
                table: "Items",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_POS_UserId",
                table: "POS",
                column: "UserId",
                unique: false,
                filter: "[UserId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_POS_UserId",
                table: "POS");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "IPadUDID",
                table: "POS",
                newName: "MacAdress");

            migrationBuilder.CreateIndex(
                name: "IX_POS_UserId",
                table: "POS",
                column: "UserId");
        }
    }
}
