﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Sanad.Services.Migrations
{
    public partial class UpdateEstablishmentColorCodePosPrinterType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PrinterType",
                table: "POS",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorCode",
                table: "establishments",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrinterType",
                table: "POS");

            migrationBuilder.DropColumn(
                name: "ColorCode",
                table: "establishments");
        }
    }
}
