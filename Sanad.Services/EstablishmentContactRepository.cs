﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class EstablishmentContactRepository : Repository<EstablishmentContact>,IEstablishmentContactRepository
    {

        private readonly SanadDBContext _db;

        public EstablishmentContactRepository(SanadDBContext db): base(db)
        {
            _db = db;
        }
        public void AddEstablishmentContact(EstablishmentContact EstablishmentContact)
        {
            EstablishmentContact.IsDeleted = false;
            EstablishmentContact.CreatedAt = DateTime.Now;
            EstablishmentContact.UpdatedAt = DateTime.Now;
            _db.Add(EstablishmentContact);
        }

        public async Task<EstablishmentContact[]> GetAllEstablishmentContactAsync(int EstablishmentId)
        {
            IQueryable<EstablishmentContact> query = _db.establishmentContacts.Where(e => e.EstablishmentId == EstablishmentId && e.IsDeleted == false); 
            return await query.ToArrayAsync();
        }

        public async Task<EstablishmentContact> GetEstablishmentContactByIdAsync(int EstablishmentId, int id)
        {
            IQueryable<EstablishmentContact> query = _db.establishmentContacts
                  .Where(e => e.EstablishmentId == EstablishmentId && e.Id == id);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<EstablishmentContact> GetEstablishmentContactByNameAsync(int EstablishmentId, string Name)
        {
            IQueryable<EstablishmentContact> query = _db.establishmentContacts
               .Where(e => e.EstablishmentId == EstablishmentId && e.Name == Name );
            return await query.FirstOrDefaultAsync();
        }

        public void RemoveEstablishmentContact(EstablishmentContact EstablishmentContact)
        {
            _db.establishmentContacts.Remove(EstablishmentContact);
        }

        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
