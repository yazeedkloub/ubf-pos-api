﻿using Sanad.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
   public interface ICompanyContactRepository : IRepository<CompanyContact>
    {
        Task<CompanyContact[]> GetAllCompanyContactAsync(int CompanyId);
        Task<CompanyContact> GetCompanyContactByIdAsync(int CompanyId, int id);
        Task<CompanyContact> GetCompanyContactByNameAsync(int CompanyId, string Name);
        void AddCompanyContact(CompanyContact category);
        void RemoveCompanyContact(CompanyContact category);
        Task<bool> SaveChangesAsync();
        bool SaveChanged();
    }
}
