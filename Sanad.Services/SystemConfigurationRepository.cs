﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using Sanad.Services.Extentions;
using Sanad.Services.Filters;
using Sanad.Services.ServiceResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class SystemConfigurationRepository : Repository<SystemConfiguration>,ISystemConfigurationRepository
    {
        private readonly SanadDBContext _context;
        public SystemConfigurationRepository(SanadDBContext context):base(context)
        {
            _context = context;
        }
        public IEnumerable<SystemConfiguration> GetAll()
        {
            return _context.SystemConfigurations.ToList();

        }
        public async Task<List<SystemConfiguration>> GetAllAsync()
        {
            var result =await _context.SystemConfigurations.Where(s => !s.ParentId.HasValue).ToListAsync();
            result.ForEach(s => {

                s = GetChildren(s);
            });
            return result;
        }

        public async Task<List<SystemConfiguration>> GetAllAsync(List<int> permissions)
        {
            if(permissions?.Any() is false)
                return await Task.FromResult(new List<SystemConfiguration>());
            var result = await _context.SystemConfigurations.Where(s => permissions.Contains(s.Id) && !s.ParentId.HasValue).ToListAsync();
            result.ForEach(s => {

                s = GetChildren(s,permissions);
            });
            return result;
        }
        public async Task<SystemConfiguration> GetByIdAsync(int id)
        {
            return await _context.SystemConfigurations.FirstOrDefaultAsync(s => s.Id == id);

        }

        public void Add<T>(T configuration) where T : class
        {
            _context.Add(configuration);

        }
        public void Remove(SystemConfiguration configuration) 
        {
           var children =  _context.SystemConfigurations.Where(s => s.ParentId == configuration.Id).ToList();

            if(children.Any())
            {

                children.ForEach(s =>
                {
                    Remove(s);
                });
            }

            _context.Remove(configuration);
        }
        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }
        public bool Save()
        {
            return (_context.SaveChanges() > 0);
        }

        public async Task<FilterResult<SystemConfiguration>> Filter(SystemConfigurationFilter filter)
        {

            if (filter == null) return null;

            var query = _context.SystemConfigurations.AsQueryable();
            //Public search
            if (!string.IsNullOrEmpty(filter.SearchQuery))
            {
                query = query.Where(s => s.NameEn.Contains(filter.SearchQuery));
            }

            if (filter.OnlyParent is true)
            {
                query = query.Where(s => !s.ParentId.HasValue);

            }
            if (filter.SortByName is true)
                query = query.Order(u => u.NameEn, filter);

            var result = await query.Pagination(filter);
            result.Result.ForEach(s =>
            {
                s =GetChildren(s);
            });

            return result;

        }
    
        public SystemConfiguration GetChildren(SystemConfiguration systemConfiguration, List<int> permissions = null)
        {
            
            if (systemConfiguration == null )
                return systemConfiguration;
            var query = _context.SystemConfigurations.AsNoTracking().Where(s => s.ParentId == systemConfiguration.Id);

            if (permissions?.Any() is true)
                query = query.Where(s => permissions.Contains(s.Id));
            //Get children
            systemConfiguration.Children.AddRange(query.ToList());
            //If children are empty return the parent
            if (!systemConfiguration.Children.Any())
                return systemConfiguration;
            //Get children for the tree
            systemConfiguration.Children.ForEach(s =>
            {
                s = GetChildren(s,permissions);
            });

            return systemConfiguration;

        }

    }
}
