﻿using Microsoft.EntityFrameworkCore;
using Sanad.Entities;
using Sanad.Services.Extentions;
using Sanad.Services.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sanad.Services
{
    public class SalesRepository : Repository<Sales>,ISalesRepository
    {
        private readonly SanadDBContext _db;
        public SalesRepository(SanadDBContext db):base(db)
        {
            _db = db;
        }
        public void AddSales(Sales sale)
        {
            Sales sales = new Sales();
            sales = _db.Sales.Where(e => e.TransactionId == sale.TransactionId).FirstOrDefault();
            if (sales == null)
            {
                sale.IsDeleted = false;
                sale.CreatedAt = DateTime.Now;
                sale.UpdatedAt = DateTime.Now;
                _db.Add(sale);
            }
            else
            {
                //To return is exist
            }
        }
        public async Task<List<Sales>> GetAllSalesAsync(string start_date, string end_date)
        {
            IQueryable<Sales> query = _db.Sales;
            //IQueryable<List<SalesItems>> query2 = _db.Sales_items.Where(a=> a.SalesId == query.;
            List<Sales> sales = new List<Sales>();
            DateTime startDate;// = DateTime.Now.AddDays(-30);
            DateTime endDate;// = DateTime.Now.AddDays(+30);
            if (start_date != "" && start_date != null && end_date != "" && end_date != null)
            { 
                startDate = DateTime.Parse(start_date);
                endDate = DateTime.Parse(end_date);
                sales = _db.Sales.Where(a => a.CreatedAt.Date >= startDate && a.CreatedAt.Date <= endDate).ToList();
            }
            else
            {
                sales = _db.Sales.ToList();
            }
            
            foreach (Sales sale in sales)
            {
                List<SalesItems> salesItems = new List<SalesItems>();
                salesItems = _db.Sales_items.Where(e => e.SalesId == sale.Id).ToList();
                sale.CartItems = salesItems;
                List<Payments> payments = new List<Payments>();
                payments = _db.Payments.Where(e => e.SalesId == sale.Id).ToList();
                sale.Payments = payments;
            }
            return sales;
        }

        public async Task<List<Sales>> Filter(SaleFilter filter)
        {
            var query = _db.Sales.Include(s => s.CartItems)
                                 .Include(s => s.Payments).AsQueryable();
            query = query.Where(s => s.IsDeleted == false);
            if(filter.POSId.HasValue)
                query = query.Where(s => s.POSId == filter.POSId);

            if(filter.StartDate.HasValue)
                query = query.Where(s => s.CreatedAt.Date >= filter.StartDate.Value.Date);

            if (filter.EndDate.HasValue)
                query = query.Where(s => s.CreatedAt.Date <= filter.EndDate.Value.Date);

            if (filter.Status.HasValue)
                query = query.Where(s => s.Status == filter.Status);

            if (filter?.Ids?.Any() is true)
                query = query.Where(s =>  filter.Ids.Contains(s.Id));

            query = query.OrderByDescending(s => s.UpdatedAt);

            var result = await query.Pagination(filter);
            return result.Result ;

        }
        public async Task<Sales> GetSalesByIdAsync(int id)
        {
            //IQueryable<Sales> query = _db.Sales
            //      .Where(e => e.Id == id);
            return await _db.Sales.Where(e => e.Id == id).Include(s => s.CartItems)
                                 .Include(s => s.Payments).FirstOrDefaultAsync();
            
        }
        public async Task<Sales> GetSaleLastId()
			{
            Sales sales = new Sales();
            sales = await _db.Sales.OrderByDescending(p => p.Id).FirstOrDefaultAsync();
            List<SalesItems> salesItems = new List<SalesItems>();
            salesItems = _db.Sales_items.Where(e => e.SalesId == sales.Id).ToList();
            sales.CartItems = salesItems;
            List<Payments> payments = new List<Payments>();
            payments = _db.Payments.Where(e => e.SalesId == sales.Id).ToList();
            sales.Payments = payments;
            return sales;
        }

       

        public void Update(Sales sale)
        {
            if(sale != null)
            {
                var itemIds = sale.CartItems.Select(i => i.ItemId);
                var items = _db.Sales_items.Where(i => i.SalesId == sale.Id && itemIds.Contains(i.ItemId) ).ToList();
                _db.Sales_items.RemoveRange(items);
                sale.UpdatedAt = DateTime.Now;
                if(sale.Payments?.Any() is true)
                {
                    var deletePayment = _db.Payments.Where(p => p.SalesId == sale.Id && !sale.Payments.Select(sp => sp.Id).Contains(p.Id)).ToList();
                    if (deletePayment.Any())
                        _db.Payments.RemoveRange(deletePayment);
                }
          
            }
        }
        //
        public void RemoveSales(Sales sales)
        {
            _db.Sales.Remove(sales);
        }
        public bool SaveChanged()
        {
            return (_db.SaveChanges() > 0);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _db.SaveChangesAsync()) > 0;
        }
    }
}
